<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Linking image path with form</title>

    <link rel="stylesheet" href="css/pure-release-0.6.0/forms-min.css">

</head>

<body>

    <h1>Important terms learnt</h1>
    <ul>
        <li>$_SERVER["REQUEST_METHOD"]</li>
        <ul>
            <li>This gets the method type of the form</li>
        </ul>
    </ul>

    <div>
        <h3>Calendar Months of the Year</h3>
    </div>

    <form action="<?php $_SERVER["PHP_SELF"]?>" method="get" name="monthsform" class="pure-form pure-form-stacked">
        <!--        The action was months.php-->

        <fieldset>


            <div class="pure-g">
                <div class="pure-u-1 pure-u-md-1-3">
                    <label for="month">Marama/Month</label>

                    <select id="month" name="monthchoice" class="pure-input-1-2">
                        <option value="1">Jan</option>
                        <option value="2">Feb</option>
                        <option value="3">March</option>
                        <option value="4">April</option>
                        <option value="5">May</option>
                        <option value="6">June</option>
                        <option value="7">July</option>
                        <option value="8">Aug</option>
                        <option value="9">Sept</option>
                        <option value="10">Oct</option>
                        <option value="11">Nov</option>
                        <option value="12">Dec</option>
                    </select>

                </div>
            </div>

            <button type="submit" class="pure-button pure-button-primary">Submit</button>
        </fieldset>
    </form>

    <hr>

<?php
//http://php.net/manual/en/function.date.php

define("IMAGE_PATH", "bigImages/");

echo "The request method we're using here is = ". $_SERVER["REQUEST_METHOD"];

echo "<pre>";
print_r($_GET);
echo "</pre>";

if($_GET) {
    $monthchoice = $_GET["monthchoice"];
//This will give a string with a number from value e.g. "1"
    
    $monthArray = ["1" => "jan.jpg", "2" => "feb.jpg", "3" => "march.jpg", "4"=>"april.jpg", "5" => "may.jpg", "6" => "june.jpg", "7"=>"july.jpg", "8"=> "aug.jpg", "9"=>"sept.jpg", "10" => "oct.jpg", "11" => "nov.jpg", "12" => "dec.jpg"];
    //This array has key number related to the image file names.
    echo "<pre>";
    print_r($monthArray);
    echo"</pre>";
              
    $imagePath = IMAGE_PATH.$monthArray[$monthchoice];
    //$imagepath = image/$monthArray["key"];
    
    echo "The picture chosen is ". $monthArray[$monthchoice]."<br>";
    
    echo "<br> The path to this image is => ".$imagePath."<br>";
    echo "<img src=$imagePath>";

    
} else {
    echo "Please select a month";
//This shows when month has not been selected yet
}

?>  


</body>

</html>
