<?php 

//PLEASE READ CAREFULLY
//Ignore those --------------yellow lines-------

//A CLASS is collection of methods and properties that are related

class Foo {
//    make a CLASS called 'Foo'
//    Usually, you capitalize class names
    
    private $employeeName;
//    We can make variables in a class
//    It can be either private or public
//    private can only accessed from INSTANCE
    
    public function __construct ($varOne) {
//        '__' is special method reserved by php
//        make a CONSTRUCTOR(method) called __construct
        
        echo "--------------------- Echoes from __construct --------------------<br>";//Ignore*
        
        echo $varOne . ' 1<br>';
//        $varOne is a variable passed in when we instantiate, which is "Jane Doe"
        
        $this->employeeName = $varOne;
//        $this-> is a KEY OPERATOR in php
//        $this-> is $myObj
//        $varOne is now saved into a variable $employeeName
        
        echo $this->employeeName . ' 2<br>';
//        You MUST use $this-> instead of $ when referring to a variable used in this method
        
        $this->setName($varOne);
//        This is same as "$this->employeeName = $varOne"
        
        echo "---------------------------------------------------- <br><br>";//Ignore*
        }
    
    public function getDate(){
        $date = date("Y-m-d H:i:s");
//        Make a variable "$date" which stores date();
//        date(); is a php function to get a time
//        Year Month Date Hour Minute Seconds
        echo "--------------------- Echoes from &#36;this->getDate() --------------------<br>";//Ignore*
        
        echo $this->employeeName . " ----------- ";
        
        echo $date ."<br>";
//        Now, getDate() function will echo the employeName and $date;
        
        echo "---------------------------------------------------- <br><br>";//Ignore*
    }
    
    private function setName($inputName){
//        Make a setName function which takes in a variable $inputName
        
        $this->employeeName = $inputName;
//        $inputName is set to employeeName
    }
    
}

?>