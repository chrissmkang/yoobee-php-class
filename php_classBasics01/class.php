<?php 
    include "includes/Foo.php";
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>Class in php</title>
    </head>

    <body>
        <h2>Whatever you see on this PHP page is written on doc.</h2>
        <h4>Open this file in a text file for descriptions</h4>

       
        <!--       PLEASE READ CAREFULLY-->
        <?php 
    
        $myObj = new Foo('Jane Doe');
        $myObj2 = new Foo('Chris Kang');
//        $myObj is an INSTANCE
//        A KEYWORD 'new' instanstiates __construct() in this class
//        This code "Instanstiating a class called foo; foo passes a reference to itself to a variable called $myObj"
//        When we instantiate, we can pass a variable called 'Jane Doe' -> (Goes into __construct())
//        An INSTANCE $myObj refers to a class "Foo" with a variable input "Jane Doe"
//        An INSTANCE $myObj2 refers to a class "Foo" with a variable input "Chris Kang"
        
        
//        Another Example
        $Heidi = new Foo ('Heidi');
        
        
        var_dump($myObj);
        echo "<br>";
        var_dump($myObj2);
        echo"<br>";
//        when you dump an INSTANCE(object) it will look like  
//        object(foo)#1 (1) {["employeeName":"foo":private=> string(8) "Jane Doe"]}
//        object(foo)#2 (1) {["employeeName":"foo":private=> string (10) "Chris Kang"]}
    
        echo "<br>";
        echo $myObj->getDate();
//        Run a method 'getDate()' inside of an instance "$myObj"
        
    ?>
    </body>

    </html>
