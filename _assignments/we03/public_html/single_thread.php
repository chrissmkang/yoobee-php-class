<?php 
include("managers/db_manager.php");
include("includes/header.php");

$thread = $db->get_thread_by_id($_GET["thread_id"]);

//WHO WROTE THIS THREAD?
$author = $db->get_user_by_id($thread[0]["user_ID"]);

//IS someone logged in?
if(isset($_SESSION["verified"])){
    $current_user = $db->get_user_detail($_SESSION["user_name"]);
    
    $voted = false;
    $votes = $db->get_number_of_votes_thread($_GET["thread_id"]);
    
    foreach($votes as $vote){
    if($vote["user_id"] == $current_user[0]["user_id"]){
        $voted = true;
    }
}
}

//comments on this thread
$comments = $db->get_comments_of_thread($_GET["thread_id"]);


//Vote is false in default


//If there is any matching of user_id in the thread, $vote becomes true;


echo "<pre>";
//var_dump($current_user[0]["user_name"]);
//var_dump($author[0]);
//var_dump($thread[0]);
//var_dump($comments[0]);
//var_dump($votes);
echo "</pre>";
?>

    <div id="single_thread">
        <div class="container">
            <div class="row">
                <div class="col-12 img_box">
                    <img id="thread_image" src="asset/threads/<?= $thread[0]["image"]; ?>" alt="">
                    <div class="detail_box">
                       <ul>
                             <li>
                                <h1 id="title"><?= $thread[0]["title"]; ?></h1>
                            </li>
                            <li>
                                <div class="details">
                                    <a id="user_name" href="userdetails.php?user=<?= $author[0]["user_id"]; ?>">
                                        by <?= $author[0]["user_name"]; ?>
                                        <div class="avatar" style="background-image: url('asset/avatars/<?= $author[0]["avatar"]; ?>')"></div>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <span id="description"><?= $thread[0]["description"]; ?></span>
                            </li>
                            
                            <!--You can only vote when logged in-->
                            <?php if($_SESSION) :?>
                            <li>
                               <h6>
                               <!--If vote is false-->
                               <?php if($voted == false) : ?>
                                <a href="scripts/process_votes.php?vote_thread=<?= $thread[0]["thread_ID"]; ?>"><i class="fa fa-heart-o" aria-hidden="true"></i> <?= count($votes); ?> votes</a>
                                <!--If vote is true-->
                                <?php else : ?>
                                <a href="scripts/process_votes.php?remove_vote_thread=<?= $thread[0]["thread_ID"]; ?>"><i id="voted" class="voted fa fa-heart" aria-hidden="true"></i> <?= count($votes); ?> votes</a>
                                <?php endif; ?>
                                </h6>
                            </li>
                                 <?php if($_SESSION["user_name"] == $author[0]["user_name"]) : ?>
                                    <!--if the user is = the post owner-->
                                    <li>
                                        <h6>
                                           <a class="button" href="edit_thread.php?edit_thread=<?=$thread["thread_ID"]; ?>">edit</a>
                                            <a class="button" href="scripts/process_thread.php?remove_thread=<?= $thread[0]["thread_ID"]; ?>">remove</a>
                                        </h6>
                                    </li>
                                <?php endif; ?>
                                
                                <? else :?>
                                
                                <li>
                                    <h6><?= $thread[0]["votes"]; ?> votes</h6>
                                </li>
                                
                            <?php endif; ?>
                            
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 comments">
                    
                    <!--COMMENTS LOOP-->
                    <?php foreach($comments as $comment) : ?>
                                        
                    <ul class="others_comments">
                        <li class="avatar" style="background-image:url('asset/avatars/<?php $comment_avatar = $db->get_user_by_id($comment['user_id']); echo $comment_avatar[0]["avatar"]; ?>')" alt="">
                        </li>
                        <li class="user_name">
                        <?php $comment_avatar = $db->get_user_by_id($comment['user_id']); echo $comment_avatar[0]["user_name"]; ?>
                        </li>
                        <li class="comment"><?= $comment["comments"]; ?></li>
                        <li class="date"><?= $comment["date"]; ?></li>
                        
                        <?php if($_SESSION) : ?>
                            <?php if($comment["user_id"] == $current_user[0]["user_id"]) : ?>
                            <li class="remove">
                                        <button class="delete_btn"><a href="scripts/process_comment.php?remove_comment=<?= $comment["comment_id"]; ?>">remove <i class="fa fa-times" aria-hidden="true"></i></a></button>
                            </li>
                            <?php endif; ?>
                        <?php endif; ?>
                    </ul>
                    <?php endforeach; ?>
                    
                    
                    <?php if(isset($_SESSION["verified"])): ?>
                    <ul class="my_comment">
                        <li class="avatar" style="background-image:url('asset/avatars/<?= $current_user[0]["avatar"]; ?>')" alt=""></li>
                        <li class="my_comment_field">
                            <form action="scripts/process_comment.php" method="post">
                                <input type="text" name="my_comment">
                                <input type="hidden" name="thread_id" value="<?= $_GET["thread_id"]; ?>">
                                <button type="submit" name="comment_submit">post</button>
                            </form>
                        </li>
                    </ul>
                    <?php endif; ?>
                    
                </div>
            </div>
        </div>

    </div>




    <!--END MAIN CONTENT-->
    <?php 
include("includes/footer.php");
?>
