<?php 
ob_start();
class Upload {
    
    public function __construct(){
    }
    
    public function upload_avatar($file_name, $file_temp) {
        move_uploaded_file($file_temp, "../asset/avatars/$file_name");
    }
    
    public function upload_thread_img($file_name, $file_temp) {
        move_uploaded_file($file_temp, "../asset/threads/$file_name");
    }
    
    public function image_validation($file_name, $name){
        
        if(isset($_POST["image"])) {
        if($_FILES[$name]["error"] > 0 ) {
            echo "where is the file";
            exit;
            }
        }
        
        $validExt = array("jpeg","jpg","png");
        $temporary = explode(".", $file_name);
        $files_ext = end($temporary);
    
        if(in_array($files_ext,$validExt) === false) {
            die ("not valid file type");
        }
    }
    
}

?>
