<?php 
include("managers/db_manager.php");
include("includes/header.php");

$users = $db->get_all_users();

if($_SESSION){
    $current_user = $db->get_user_detail($_SESSION["user_name"]);
}

?>

<div id="about">
    <div class="container">
        <div class="row">
           <h1>About</h1>
           <p>Doodle has been founded for the love of art and design. You are free to share your artistic illustrations and creative designs, which could be voted by other users. The more votes you get, the easier other people get to view your work, which will enable you to be found by recruiters around the world!</p>
           <p>We would love to accept all sorts of categories including any doodles to digital paint, manga, concept sketch, illustration, and tutorials. If you have any suggestions for other categories, we are very happy to add more categories in the future.</p>
           <p>Please feel free to email at <a href="mailto:admin@doodles.com">admin@doodles.com</a> if you have other enqueries or questions.</p>
           <p>Doodle Team</p>
        </div>
    </div>
</div>






    <!--END MAIN CONTENT-->
    <?php 
include("includes/footer.php");
?>
