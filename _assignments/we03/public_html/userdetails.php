<?php 
include("managers/db_manager.php");
include("includes/header.php");

$categories = $db->get_enabled_categories();
$users = $db->get_all_users();
$user_thread = $db->get_user_threads($_GET["user"]);


if(isset($_SESSION["verified"])){
    //WHO IS LOGGED IN?
    $current_user = $db->get_current_user($_SESSION["user_name"]);
}

if(isset($_GET["user"])){
    //WHO'S PAGE IS THIS
    $user_page = $db->get_user_by_id($_GET["user"]);
//        *************************************
}

$voted = false;

//echo "<pre>";
//var_dump($current_user);
//echo "<pre>";

?>

        <div id="userdetails_page">
            <div class="container">
                <div class="row">
                    
                    <ul class="col-12 my_details">
                        <li class="user_detail_avatar" style="background-image : url('asset/avatars/<?= $user_page[0]["avatar"]; ?>')">
                        </li>
                        <li>
                            <h3><?= $user_page[0]["first_name"]; ?>
                                    <?= $user_page[0]["last_name"]; ?></h3>
                        </li>
                        <li>
                            <h6><?= $user_page[0]["user_name"]; ?></h6>
                        </li>
                        <li>
                            <h6><?= $user_page[0]["email"]; ?></h6>
                        </li>
                          <?php if(isset($_SESSION["verified"])) : ?>
                           
                            <?php if($current_user[0]["user_id"] == $user_page[0]["user_id"]) : ?>
                            <!--SHOW EDIT BUTTON ONLY WHEN LOGGED IN USER IS MATCHED WITH THE USER PAGE-->
                        <li>
                            <button><a href="useredit.php?user=<?= $current_user[0]["user_id"]; ?>">EDIT PROFILE</a></button>
                             <button><a href="new_post.php?user=<?= $current_user[0]["user_id"]; ?>">NEW POST</a></button>
                        </li>
                            <?php endif; ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
            
            <!-- USER GALLERY -->
            <div id="gallery">
                <div class="row">
                    <?php foreach($user_thread as $thread) : ?>
                    <div class="col-3 col-6-sm post">
                        <a href="single_thread.php?thread_id=<?= $thread["thread_ID"] ?>">
                           <img src="asset/threads/<?= $thread["image"] ?>" alt="">
                            <div class="details">
                                <div>
                                    <h3><?= $thread["title"] ?></h3>
                                    <h6><a href="userdetails.php?user=<?php $user_id = $db->get_user_by_id($thread["user_ID"]); echo $user_id[0]["user_id"] ?>"><?php $user_id = $db->get_user_by_id($thread["user_ID"]); echo $user_id[0]["user_name"] ?></a></h6>
                                    
                                    <!--VOTES-->
                                    
                                    <?php if($_SESSION) : ?>
                                        <?php $votes = $db->get_number_of_votes_thread($thread["thread_ID"]); 
                                        ?>
                                                                                
                                        <?php if(in_array($current_user[0]["user_id"], array_column($votes,"user_id"))) :?>
                                            <a href="scripts/process_votes.php?remove_vote_thread=<?= $thread["thread_ID"]; ?>"><i id="voted" class="voted fa fa-heart" aria-hidden="true"></i> <?= count($votes); ?> votes</a>
                                        <?php else :?>
                                           <a href="scripts/process_votes.php?vote_thread=<?= $thread["thread_ID"]; ?>"><i class="fa fa-heart-o" aria-hidden="true"></i> <?= count($votes); ?> votes</a>
                                        <?php endif;?>
                                        
                                    
                                    <?php else :?>
                                    
                                       <h6><?= $thread["votes"]; ?> votes</h6>
                                   
                                   <?php endif; ?>
                                    
                                    <!--IF you're logged in AND-->
                                    <?php if(isset($_SESSION["verified"])) :?>
                                    <!--Your ID is same as the user's page-->
                                        <?php if($user_page[0]["user_id"] == $current_user[0]["user_id"]) : ?>
                                        <button class="edit_btn"><a href="edit_thread.php?edit_thread=<?=$thread["thread_ID"]; ?>">edit</a></button>
                                        <button class="delete_btn"><a href="scripts/process_thread.php?remove_thread=<?= $thread["thread_ID"]; ?>">remove <i class="fa fa-times" aria-hidden="true"></i></a></button>
                                        <?php endif; ?>
                                    <? endif; ?>

                                    
                            </div>
                        </div>
                    </a>
                </div>
                <?php endforeach; ?>

            </div>

        </div>

    <script src="scripts/index.js"></script>


        </div>


        <?php 
include("includes/footer.php");
?>
