<?php 
include("managers/db_manager.php");
include("includes/header.php");

$categories = $db->get_all_categories();
$users = $db->get_all_users();
$threads = $db->get_all_threads();

//echo "<pre>";
//var_dump($categories);
//echo "</pre>";

?>
    <!--MAIN CONTENT-->

    <div id="admin_page">
        <div class="container">
            <div class="row">
                <h1>Admin</h1>
            </div>
            <div class="row">
                <div class="col-3">
                    <h4>Users</h4>
                </div>
                <div class="col-9 right">
                    <button class="edit_btn edit_users"><i class="fa fa-caret-down" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
            <div class="row edit_div admin_users hidden">
           <table class="users_tbl">
               <thead>
                  <tr>
                      <td>Avatar</td>
                      <td>ID</td>
                       <td>User Name</td>
                       <td>Full Name</td>
                       <td>Email</td>
                       <td>Delete</td>
                  </tr>
               </thead>
               <tbody>
                  <?php foreach($users as $user) :?>
                   <tr>
                       <td><img src="asset/avatars/<?= $user["avatar"] ?>" alt=""></td>
                       <td><?= $user["user_id"] ?></td>
                       <td><?= $user["user_name"] ?></td>
                       <td><?= $user["first_name"] ?> <?= $user["last_name"] ?></td>
                       <td><?= $user["email"] ?></td>
                       <td><button><a href="./managers/admin_manager.php<?= '?remove_user='. $user["user_id"] ?>">DELETE</a></button></td>
                   </tr>
                   <?php endforeach; ?>
               </tbody>
           </table>
           
            </div>
            <hr>
            <div class="row">
                <div class="col-3">
                    <h4>Categories</h4>
                </div>
                <div class="col-9 right">
                    <button class="edit_btn edit_categories"><i class="fa fa-caret-down" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
            <div class="row edit_div admin_categories hidden">
                <ul>
                    <?php foreach($categories as $category) :?>
                        <li class="<?= $category["status"]; ?>">
                            <a href="<?= './managers/admin_manager.php'.'?update_category_status='.$category["category_id"]; ?>" target="_self">
                                <h6><?= $category["category_name"]; ?></h6>
                                <span><?= ($category["status"] == "true")?"Enabled" : "Disabled"; ?></span>
                            </a>
                        </li>
                        <?php endforeach; ?>
                </ul>
                <form action="./managers/admin_manager.php" method="post">
                    <fieldset>
                       <label for="category_name">New Category</label>
                        <input type="text" name="category_name">
                        <button type="submit" name="category_add_submit">Add</button>
                    </fieldset>
                </form>
            </div>
            <hr>
            <div class="row">
                <div class="col-3">
                    <h4>Threads</h4>
                </div>
                <div class="col-9 right">
                    <button class="edit_btn edit_threads"><i class="fa fa-caret-down" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
            <div class="row edit_div admin_threads hidden">
             <table class="threads_tbl">
               <thead>
                  <tr>
                      <td>Thread ID</td>
                       <td>User Name</td>
                        <td>Title</td>
                       <td>Image</td>
                       <td>Delete</td>
                  </tr>
               </thead>
               <tbody>
              <?php foreach($threads as $thread) : ?>
                    <tr>
                         <td><?= $thread["thread_ID"] ?></td>
                         <td><?php $user_id = $db->get_user_by_id($thread["user_ID"]); echo $user_id[0]["user_name"];?></td>
                        <td><?= $thread["title"] ?></td>
                         <td><img src="asset/threads/<?= $thread["image"] ?>" alt="thread_img"></td>
                         <td><button><a href="./managers/admin_manager.php<?= '?remove_thread='. $thread["thread_ID"] ?>" target="_self">DELETE</a></button></td>
                     </tr>
               <?php endforeach; ?>
                 </tbody>
                </table>
            </div>
        </div>
    </div>
    
<script src="scripts/admin_page.js"></script>


    <!--END MAIN CONTENT-->
    <?php 
include("includes/footer.php");
?>
