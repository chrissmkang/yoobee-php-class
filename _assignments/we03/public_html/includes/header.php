<?php 
session_start();
ob_start();


$loggedin = false;
$admin = false;

$db = new db_manager();


if(isset($_SESSION["verified"]) && isset($_SESSION["user_name"])){
    $loggedin = true;
    $logged_user = $db->get_user_detail($_SESSION["user_name"]);
    
    if($logged_user[0]["privilege"] == "1"){
        $admin = true;
    } else {
        $admin = false;
    }
    
} else if($_SESSION){
    $loggedin = false;
}



?>


    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>Welcome to doodle!</title>
        <link rel="stylesheet" href="css/grid/simple-grid.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome-4.7.0/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/styles.css" type="text/css">
    </head>

    <body>

        <!--NAVIGATION-->
        <div class="FLEX-COLUMN">

            <nav class="container">
                <div class="row">
                    <!--LOGO-->
                    <div class="col-2 col-2-sm">
                        <a class="logo" href="index.php">doodle</a>
                    </div>

                    <!--SEARCHBOX-->
                    <div class="col-5 col-5-sm">
                        <form action="">
                            <input class="searchbox" type="text" placeholder="search by thread names">
                        </form>
                    </div>

                    <!--NAVIGATION-->
                    <div class="col-5 col-5-sm right">
                        <ul>
                            <?php if($loggedin) : ?>
                                <!--LOGGED IN-->
                                <li>
                                   <?php if($admin) : ?>
                                   
                                   
                                    <!--IF ADMIN-->
                                    <a href="admin_page.php">
                                         <?= $_SESSION["user_name"]; ?>
                                    </a>
                                    <?php else :?>
                                    
                                    
                                    
                                    <!--IF NORMAL USERS-->
                                    <a href="userdetails.php?user=<?= $logged_user[0]["user_id"]; ?>">
                                         <div class="nav_avatar" style="background-image: url('./asset/avatars/<?= $logged_user[0]["avatar"]; ?>')"></div>
                                         <?= $_SESSION["user_name"]; ?>
                                    </a>
                                    <?php endif; ?>
                                </li>
                                <li>
                                    <form action="scripts/process_logout.php" method="post">
                                        <button type="submit" name="logout_submit"><a>Logout</a></button>

                                    </form>
                                </li>
                                <?php else : ?>
                                   
                                   
                                   
                                    <!--LOGGED OUT-->
                                    <li><a href="register.php">Register</a></li>
                                    <li><a href="login.php">Login</a></li>

                                    <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </nav>
