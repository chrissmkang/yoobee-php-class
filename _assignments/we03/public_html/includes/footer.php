<footer>
   <div class="container">
       <div class="row">
           <div class="col-6">
               <ul>
                   <li><a href="index.php">Home</a></li>
                   <li><a href="about.php">About</a></li>
                    <?php if($loggedin) : ?>

                       <?php if($admin) : ?>
                           <li><a href="admin_page.php">Admin</a></li>
                       <?php else :?>
                           <li><a href="userdetails.php?user=<?= $logged_user[0]["user_id"]; ?>">My Page</a></li>
                           <li><a href="useredit.php?user=<?= $logged_user[0]["user_id"]; ?>">Edit Profile</a></li>
                           <li><a href="new_post.php?user=<?= $logged_user[0]["user_id"]; ?>">New Post</a></li>
                       <?php endif; ?>
                   <?php endif;?>
              </ul>
           </div>
           <div class="col-6 right">
               <ul>
                   <li>
                   <a href="mailto:admin@doodles.com">admin@doodles.com</a></li>
               </ul>
           </div>
       </div>
   </div>
    <p class="copyrights">©copyrights reserved by Doodles</p>
</footer>
</div>
</body>

</html>
