<?php 
ob_start();
include("../class/image_upload.php");
include("../managers/db_manager.php");
include("../managers/password_manager.php");

if(isset($_POST["register_submit"])){
    //    GET ALL THE INPUT DATA INTO VARIABLES !!!!!!
    
    
    $user_name = $_POST["user_name"];
    $user_name = filter_var($user_name, FILTER_SANITIZE_STRING);
    
    $first_name = $_POST["first_name"];
    $first_name = filter_var($first_name, FILTER_SANITIZE_STRING);
    $first_name = ucfirst(strtolower($first_name));
    
    $last_name = $_POST["last_name"];
    $last_name = filter_var($last_name, FILTER_SANITIZE_STRING);
    $last_name = ucfirst(strtolower($last_name));

    
    $email = $_POST["email"];
    $email = filter_var($email, FILTER_SANITIZE_STRING);

    $password = $_POST["password"];
    $password_manager = new Password_manager();
    $password = $password_manager->hashPW($password);
    
//    echo "<pre>";
//    var_dump($_FILES);
//    echo "</pre>";
    
   
    if($_FILES["avatar"]["name"] == ""){
        $avatar = "default.jpg";
    } else {
        $avatar = $_FILES["avatar"]["name"];
        $temp_avatar = $_FILES["avatar"]["tmp_name"];
        
        $Upload = new Upload();
        $Upload->image_validation($avatar, "avatar");
        $Upload->upload_avatar($avatar, $temp_avatar);
    } 
    
    
    
    if(empty($user_name) || empty($first_name) || empty($last_name) || empty($email) || empty($password)){
        $failedURL = $_SERVER["HTTP_REFERER"].'?empty=false';
        header("location:$failedURL");
    } else {
        
        $db_manager = new db_manager();
    
        $registering_user = $db_manager->register_user($user_name, $first_name, $last_name, $password, $email, $avatar);

        $registration_result = ($registering_user)? '../registration_thanks.php?registration=true' : $_SERVER["HTTP_REFERER"].'?registration=false';

        header("Location: $registration_result");
    }
    
    
    
    
}


?>
