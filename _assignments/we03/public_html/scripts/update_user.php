<?php 
session_start();
ob_start();
include("../class/image_upload.php");
include("../managers/db_manager.php");
include("../managers/session_manager.php");
include("../managers/password_manager.php");


if(isset($_POST["edit_submit"])){
    //    GET ALL THE INPUT DATA INTO VARIABLES !!!!!!
    
    if($_POST["user_name"] == ""){
        $user_name = $_POST["user_name_original"];
    } else {
        $user_name = $_POST["user_name"];
        $user_name = filter_var($user_name, FILTER_SANITIZE_STRING);   
    }
    
    if($_POST["first_name"] == ""){
        $first_name = $_POST["first_name_original"];
    } else {
        $first_name = $_POST["first_name"];
        $first_name = filter_var($first_name, FILTER_SANITIZE_STRING);
        $first_name = ucfirst(strtolower($first_name));   
    }
    
    if($_POST["last_name"] == ""){
        $last_name = $_POST["last_name_original"];
    } else {
        $last_name = $_POST["last_name"];
        $last_name = filter_var($last_name, FILTER_SANITIZE_STRING);
        $last_name = ucfirst(strtolower($last_name));
    }
    
    if($_POST["email"] == ""){
        $email = $_POST["email_original"];
    } else {
        $email = $_POST["email"];
        $email = filter_var($email, FILTER_SANITIZE_STRING);
    }

    
//    echo "<pre>";
//    var_dump($_FILES);
//    echo "</pre>";
    
    if(isset($_POST["avatar"])) {
        if($_FILES["avatar"]["error"] > 0 ) {
            echo "where is the file";
            exit;
        }
        $validExt = array("jpeg","jpg","png");
        $temporary = explode(".", $_FILES["avatar"]["name"]);
        $files_ext = end($temporary);
    
        if(in_array($files_ext,$validExt) === false) {
            die ("not valid file type");
        }
    }
   
    if($_FILES["avatar"]["name"] == ""){
        $avatar = $_POST["avatar_original"];
    } else {
        $avatar = $_FILES["avatar"]["name"];
        $temp_avatar = $_FILES["avatar"]["tmp_name"];
        
        $Upload = new Upload();
        $Upload->upload_avatar($avatar, $temp_avatar);
    } 
     
    $db_manager = new db_manager();
    
    
    $current_user = $db_manager->get_user_detail($_POST["user_name_original"]);
    
    echo"<pre>";
    var_dump($current_user);
    echo"</pre>";
    
    
    $user_id = $current_user[0]["user_id"];
    
    
    $updating_user = $db_manager->update_user($user_id, $user_name, $first_name, $last_name, $email, $avatar);
    
    $session_manager = new Session_manager();
    $session_manager->update_user($user_name);
    
    $updating_result = ($updating_user)? "../userdetails.php?user=$user_id" : $_SERVER["HTTP_REFERER"].'?user_edit=false';
    
    
    
    
    header("Location: $updating_result");
    
    
}


?>
