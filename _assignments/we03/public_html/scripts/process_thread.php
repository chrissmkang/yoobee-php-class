<?php 
session_start();
ob_start();
include("../class/image_upload.php");
include("../managers/db_manager.php");
include("../managers/user_manager.php");

//echo "<pre>";
//var_dump($_POST);
//var_dump($_FILES);
//echo "</pre>";

$db_manager = new db_manager();
$current_user = $db_manager->get_user_detail($_SESSION["user_name"]);

$user_id = $current_user[0]["user_id"];

if(isset($_POST["thread_submit"]) && isset($_FILES)){
    
    if(!empty($_POST["title"])){
        $title = $_POST["title"];
        $title = filter_var($title, FILTER_SANITIZE_STRING);    
    } else {
        $title = "No Title";
    }
    
    $category = $_POST["category"];
    
    if(!empty($_POST["description"])){
        $description = $_POST["description"];
        $description = filter_var($description, FILTER_SANITIZE_STRING);
    } else {
        $description = "No description";
    }
   
    if($_FILES["image"]["name"] == ""){
         $return = $_SERVER["HTTP_REFERER"].'&image_load=false';
        header("Location: $return");
        die ("no image");
       
    } else {
        $image = $_FILES["image"]["name"];
        $temp_image = $_FILES["image"]["tmp_name"];
        
        $Upload = new Upload();
        $Upload->image_validation($image, "image");
        
        $image = $user_id."_".$image;
        $Upload->upload_thread_img($image, $temp_image);
    } 
    
    $user_manager = new User_manager();
    $user_manager->upload_thread($category, $title, $image, $description, $user_id);
    
}

if(isset($_POST["edit_thread"])){
    
    $thread_id = $_POST["thread_id"];
    
    if($_POST["title"] == ""){
        $title = $_POST["title_original"];
    } else {
        $title = $_POST["title"];
        $title = filter_var($title, FILTER_SANITIZE_STRING);   
    }
    
    $category = $_POST["category"];
    
    if(!empty($_POST["description"])){
        $description = $_POST["description"];
        $description = filter_var($description, FILTER_SANITIZE_STRING);
    } else {
        $description = "No description";
    }
    
    echo $title;
    echo $category;
    echo $description;
    
    $user_manager = new User_manager();
    $user_manager->edit_thread($title,$category,$description,$thread_id);
    
}

if(isset($_GET["remove_thread"])){
    $user_manager = new User_manager();
    $user_manager->remove_thread($_GET["remove_thread"], $user_id);
}


?>