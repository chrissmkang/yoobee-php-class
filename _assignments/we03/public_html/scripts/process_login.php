<?php 
session_start();
ob_start();
include("../managers/db_manager.php");
include("../managers/password_manager.php");
include("../managers/session_manager.php");
    $db_manager = new db_manager();
    $password_manager = new Password_manager();
    $session_manager = new Session_manager();

//***********************************************************


if(isset($_POST["login_submit"])){
    
    $user_name = $_POST["user_name"];
    $user_name = filter_var($user_name, FILTER_SANITIZE_STRING);
    
    $password = $_POST["password"];
    $storedPassword = $db_manager->get_password($user_name);
    //password = user input password from login form
    //storedpassword = hashed password of user input from login form
    
    
    $verifiedPassword = $password_manager->check_password($password, $storedPassword);
    //If the password match verifiedPassword = true. if not, false;
    
    
    //IF the password matches to the user's password:
    if($verifiedPassword) {
        $session_manager->verify_user($user_name);
        //session starts, and takes to userdetail page.
        $session_manager->returnHome();
    } else {
        //IF NOT show error
        $login_fail = $_SERVER["HTTP_REFERER"]."?login=false";
        header("Location: $login_fail");
        }
}
?>
