<?php
session_start();
ob_start();
include("../managers/db_manager.php");
include("../managers/user_manager.php");

$db = new db_manager();

$current_user = $db->get_user_detail($_SESSION["user_name"]);




if(isset($_POST["comment_submit"])){
    
    if(!empty($_POST["my_comment"])){
        $comment = $_POST["my_comment"];
        $comment = filter_var($comment, FILTER_SANITIZE_STRING);

        $user_id = $current_user[0]["user_id"];
        
        $thread_id = $_POST["thread_id"];
        
        $user_manager = new User_manager();
        $user_manager->upload_comment($user_id, $thread_id, $comment);
    
    } else {
        //When comment is empty, do nothing
        $return = $_SERVER["HTTP_REFERER"].'&comment=false';
        header("Location: $return");
        die("no comment");
    }
}


if(isset($_GET["remove_comment"])){
    $user_manager = new User_manager();
    $user_manager->remove_comment($_GET["remove_comment"], $user_id);
}



?>