<?php
session_start();
ob_start();
include("../managers/db_manager.php");
include("../managers/user_manager.php");

$db = new db_manager();


$current_user = $db->get_user_detail($_SESSION["user_name"]);



if(isset($_GET["vote_thread"])){
    
    $thread_id = $_GET["vote_thread"];
        
    $user_id = $current_user[0]["user_id"];
    
//    echo "This is a thread_id = ".$thread_id."<br>";
//    echo "This is a logged in user_id = ".$user_id."<br>";
    
    
    $user_manager = new User_manager();
    $user_manager->update_vote($thread_id, $user_id);

}


if(isset($_GET["remove_vote_thread"])){
    
    $thread_id = $_GET["remove_vote_thread"];
    
    $user_id= $current_user[0]["user_id"];
    
    $user_manager = new User_manager();
    $user_manager->remove_vote($thread_id, $user_id);
    
}



?>