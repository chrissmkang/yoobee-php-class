document.addEventListener("DOMContentLoaded", myDOMloaded, false);

function myDOMloaded(e) {
    toggle_admin_categories();
    toggle_admin_user();
    toggle_admin_threads();

}

function toggle_admin_categories() {
    var edit_button = document.querySelector(".edit_categories");
    var admin_categories = document.querySelector(".admin_categories");

    edit_button.addEventListener("click", function toggle_hidden(e) {
        admin_categories.classList.toggle("hidden");
    }, false);
}

function toggle_admin_user() {
    var edit_button = document.querySelector(".edit_users");
    var admin_user = document.querySelector(".admin_users");

    edit_button.addEventListener("click", function toggle_hidden(e) {
        admin_user.classList.toggle("hidden");

    }, false);
}

function toggle_admin_threads() {
    var edit_button = document.querySelector(".edit_threads");
    var admin_threads = document.querySelector(".admin_threads");

    edit_button.addEventListener("click", function toggle_hidden(e) {
        admin_threads.classList.toggle("hidden");

    }, false);
}
