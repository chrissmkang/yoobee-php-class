document.addEventListener("DOMContentLoaded", myDOMloaded, false);

function myDOMloaded(e) {
    post_hover();

}

function post_hover() {
    var details = document.querySelectorAll(".post");
    var img = document.querySelectorAll(".post img");

    for (var i = 0; i < details.length; i++) {
        details[i].addEventListener("mouseover", function toggle_thread_animation(e) {

            for (var j = 0; j < details.length; j++) {
                if (e.currentTarget === details[j]) {
                    img[j].classList.add("img_hover");
//                    console.log(img[j]);
                }
            }
        }, false);
    }

    for (var i = 0; i < details.length; i++) {
        details[i].addEventListener("mouseout", function toggle_thread_animation(e) {

            for (var j = 0; j < details.length; j++) {
                if (e.currentTarget === details[j]) {
                    img[j].classList.remove("img_hover");
                    //console.log(img[j]);
                }
            }
        }, false);
    }


}
