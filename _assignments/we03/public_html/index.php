<?php 
include("managers/db_manager.php");
include("includes/header.php");

$categories = $db->get_enabled_categories();
$users = $db->get_all_users();

if(isset($_GET["category_id"])){
    $threads = $db->get_thread_by_category_id($_GET["category_id"]);
} else{
    $threads = $db->get_all_threads();
}

if($_SESSION){
    $current_user = $db->get_user_detail($_SESSION["user_name"]);
}


?>

    <!--MAIN CONTENT-->

    <div id="index">

        <!--       CATEGORIES-->
        <!--        LOOP THROUGH CATEGORIES-->
        <ul class="categories">
            <div class="container flex">
              <li><a href="./index.php">All</a></li>
               <?php foreach($categories as $category) : ?>
                <li><a href="./index.php?category_id=<?= $category["category_id"]; ?>"><?= $category["category_name"] ?></a></li>
                <?php endforeach; ?>
                
            </div>
        </ul>
        <!--        END CATEGORIES-->

        <div id="gallery">
 <?php
//        echo "<pre>";
//        var_dump($_SESSION);
//        var_dump($current_user[0]);
//        echo "</pre>";
    ?>
            <div class="row">
                <?php foreach($threads as $thread) : ?>
                <?php  $user = $db->get_user_by_id($thread["user_ID"]); ?>
                <div class="col-3 col-6-sm post">                       
                       <a href="single_thread.php?thread_id=<?= $thread["thread_ID"] ?>">
                          <img src="asset/threads/<?= $thread["image"]; ?>" alt="<?= $thread["image"]; ?>">
                           
                            <div class="details">
                                <div>
                                    <!--Title-->
                                    <h3><?= $thread["title"] ?></h3>
                                    <!--User Name-->
                                    <h6><a href="userdetails.php?user=<?= $user[0]["user_id"] ?>"><?= $user[0]["user_name"] ?></a></h6>
                                    <!--Votes-->
                                    
                                    
                                    <?php if($_SESSION) : ?>
                                        <?php $votes = $db->get_number_of_votes_thread($thread["thread_ID"]); 
                                        ?>
                                                                                
                                        <?php if(in_array($current_user[0]["user_id"], array_column($votes,"user_id"))) :?>
                                            <a href="scripts/process_votes.php?remove_vote_thread=<?= $thread["thread_ID"]; ?>"><i id="voted" class="voted fa fa-heart" aria-hidden="true"></i> <?= count($votes); ?> votes</a>
                                        <?php else :?>
                                           <a href="scripts/process_votes.php?vote_thread=<?= $thread["thread_ID"]; ?>"><i class="fa fa-heart-o" aria-hidden="true"></i> <?= count($votes); ?> votes</a>
                                        <?php endif;?>
                                        
                                    
                                    <?php else :?>
                                    
                                       <h6><?= $thread["votes"]; ?> votes</h6>
                                   
                                   <?php endif; ?>
                                   
                                   
                                

                                   
                                   
                                   
                                   
                                   <?php if($_SESSION) : ?>
                                       <?php if($_SESSION["user_name"] == $user[0]["user_name"]) : ?>
                                       <!--If user, remove button-->
                                       
                                        <button class="edit_btn"><a href="edit_thread.php?edit_thread=<?=$thread["thread_ID"]; ?>">edit</a></button>
                                        
                                        <button class="delete_btn"><a href="scripts/process_thread.php?remove_thread=<?= $thread["thread_ID"]; ?>">remove <i class="fa fa-times" aria-hidden="true"></i></a></button>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                            </a>
                </div>
                <?php endforeach; ?>

            </div>

        </div>


    </div>
    
    <script src="scripts/index.js"></script>
    


    <!--END MAIN CONTENT-->
    <?php 
include("includes/footer.php");
?>
