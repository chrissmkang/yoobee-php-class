<?php 
include("managers/db_manager.php");
include("includes/header.php");

$db_manager = new db_manager();
$categories = $db_manager->get_all_categories();



?>

    <div id="new_thread">

        <div class="container">
            <h1>New Thread</h1>

            <form action="scripts/process_thread.php" method="post" enctype="multipart/form-data">


                        <fieldset class="row">
                            <label class="col-2" for="title">Title</label>
                            <input class="col-8" type="text" name="title" autofocus>
                        </fieldset>
                           
                           
                        <fieldset class="row">
                            <label class="col-2" for="category">Category</label>
                            <select class="col-8" name="category">
                               <?php foreach($categories as $category) : ?>
                                <option value="<?= $category["category_id"]; ?>"><?= $category["category_name"]; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </fieldset>
                        
                        <fieldset class="row">
                            <label class="col-2" for="image">Image</label>
                            <input type="file" class="col-3" name="image">
                            <?php if(isset($_GET["image_load"])): ?>
                            <label class="col-5 warning">Image is missing</label>
                            
                            <?php endif; ?>
                        </fieldset>
                        
                        <fieldset class="row">
                            <label class="col-2" for="description">Description</label>
                            <textarea name="description" id="" cols="30" rows="10" class="col-8"></textarea>
                        </fieldset>
                        
                        
                        <fieldset class="row">
                           <div class="col-2"></div>
                            <div class="col-8">
                                <button type="submit" name="thread_submit">Post</button>
                            </div>
                        </fieldset>


            </form>
        </div>




    </div>


    <?php 
include("includes/footer.php");
?>
