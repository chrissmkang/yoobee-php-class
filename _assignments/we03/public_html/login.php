<?php 
include("managers/db_manager.php");
include("includes/header.php");
?>

    <div id="login">

        <div class="container">
            <h1>Login</h1>

            <form action="scripts/process_login.php" method="post">


                <?php if(isset($_GET["login"])) : ?>
                    <!--When log in fails-->
                    <fieldset class="row">
                        <h5 class="warning center">The Username does not match password!</h5>
                    </fieldset>
                    <!--Login Fails-->
                    <?php endif; ?>

                        <fieldset class="row">
                            <label class="col-4" for="user_name">User Name</label>
                            <input class="col-5" type="text" name="user_name" autofocus required>
                        </fieldset>
                        <fieldset class="row">
                            <label class="col-4" for="password">Password</label>
                            <input class="col-5" type="password" name="password" required>
                        </fieldset>
                        <fieldset class="row">
                            <button type="submit" name="login_submit">Login</button>
                        </fieldset>


            </form>
        </div>




    </div>


    <?php 
include("includes/footer.php");
?>
