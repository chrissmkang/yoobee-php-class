<?php 
include("managers/db_manager.php");
include("includes/header.php");

$current_user = $db->get_user_by_id($_GET["user"]);

//var_dump($current_user);

?>

    <div id="register">
        <div class="container">
            <h1>Edit User Details</h1>

            <form action="scripts/update_user.php" method="post" enctype="multipart/form-data">
                <!--***************** PUT REQUIRED FOR FIELDS *********************-->
                <fieldset class="row">
                   <div class="col-12 center"><img class="avatar" src="asset/avatars/<?= $current_user[0]["avatar"]; ?>" alt="">
                       </div>                    
                       <label class="col-4" for="avatar">Avatar:</label>
                       <input class="col-5" type="hidden" name="avatar_original" value="<?= $current_user[0]["avatar"]; ?>">
                    <input class="col-5" type="file" name="avatar">
                </fieldset>
                
                <fieldset class="center">
                    <span>Avatar image must be jpeg, jpg, or png</span>
                </fieldset>
                
                <fieldset class="row">
                    <label class="col-4" for="user_name">User Name : </label>
                    <input class="col-5" type="hidden" name="user_name_original" value="<?= $current_user[0]["user_name"]; ?>">
                    <input class="col-5" type="text" name="user_name" placeholder="<?= $current_user[0]["user_name"]; ?>">
                </fieldset>
                
                <fieldset class="row">
                    <label class="col-4" for="first_name">First Name : </label>
                    <input class="col-5" type="hidden" name="first_name_original" value="<?= $current_user[0]["first_name"]; ?>">
                    <input class="col-5" type="text" name="first_name" placeholder="<?= $current_user[0]["first_name"]; ?>">
                </fieldset>
                
                <fieldset class="row">
                    <label class="col-4" for="last_name">Last Name : </label>
                    <input class="col-5" type="hidden" name="last_name_original" value="<?= $current_user[0]["last_name"]; ?>">
                    <input class="col-5" type="text" name="last_name" placeholder="<?= $current_user[0]["last_name"]; ?>">
                </fieldset>
                
                <fieldset class="row">
                    <label class="col-4" for="email">Email : </label>
                    <input class="col-5" type="hidden" name="email_original" value="<?= $current_user[0]["email"]; ?>">
                    <input class="col-5" type="email" name="email" placeholder="<?= $current_user[0]["email"]; ?>">
                </fieldset>
                
                
                <?php if(isset($_GET["registration"])) : ?>
                    <fieldset class="center warning">
                        <span class="warning">Username Or Email already exists!</span>
                    </fieldset>
                    <?php endif;?>
                        <fieldset class="row">
                            <div class="col-6">
                                <button type="submit" name="edit_submit">Edit</button>
                            </div>
                            <div class="col-6">
                                <button type="reset" name="edit_reset">Reset</button>
                            </div>
                        </fieldset>



            </form>
        </div>
    </div>


    <?php 
include("includes/footer.php");
?>
