<?php 
include("managers/db_manager.php");
include("includes/header.php");
?>

    <div id="register">
        <div class="container">
            <h1>Register</h1>

            <form action="scripts/process_registration.php" method="post" enctype="multipart/form-data">
                <!--***************** PUT REQUIRED FOR FIELDS *********************-->
                <fieldset class="row">
                    <label class="col-4" for="user_name" required>User Name : </label>
                    <input class="col-5" type="text" name="user_name">
                </fieldset>
                <fieldset class="row">
                    <label class="col-4" for="first_name" required>First Name : </label>
                    <input class="col-5" type="text" name="first_name">
                </fieldset>
                <fieldset class="row">
                    <label class="col-4" for="last_name" required>Last Name : </label>
                    <input class="col-5" type="text" name="last_name">
                </fieldset>
                <fieldset class="row">
                    <label class="col-4" for="email" required>Email : </label>
                    <input class="col-5" type="email" name="email">
                </fieldset>
                <fieldset class="row">
                    <label class="col-4" for="password" required>Password : </label>
                    <input class="col-5" type="password" name="password">
                </fieldset>
                <fieldset class="row">
                    <label class="col-4" for="avatar">Avatar:</label>
                    <input class="col-5" type="file" name="avatar">
                </fieldset>
                <fieldset class="center">
                    <span>Avatar image must be jpeg, jpg, or png</span>
                </fieldset>
                <?php if(isset($_GET["registration"])) : ?>
                    <fieldset class="center warning">
                        <span class="warning">Username Or Email already exists!</span>
                    </fieldset>
                    <?php endif;?>
                    
                    <?php if(isset($_GET["empty"])) : ?>
                    <fieldset class="center warning">
                        <span class="warning">Please fill in the details!</span>
                    </fieldset>
                    <?php endif;?>
                        <fieldset class="row">
                            <div class="col-6">
                                <button type="submit" name="register_submit">Register</button>
                            </div>
                            <div class="col-6">
                                <button type="reset" name="register_reset">Reset</button>
                            </div>
                        </fieldset>



            </form>
        </div>
    </div>


    <?php 
include("includes/footer.php");
?>
