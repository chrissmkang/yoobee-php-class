<?php 
include_once("db_manager.php");
ob_start();

//$_GET["update_category_status"] == category ID #
    
    class User_manager {
    
    public function __construct(){        
    }
    
    public function upload_thread($category, $title, $image, $description, $user_id){
        
        $sql = "INSERT INTO `threads`(`category_ID`, `title`, `image`, `description`, `user_ID`, `votes`, `status`) VALUES ('$category', '$title', '$image', '$description' ,'$user_id', '0' ,'true')";
        
        $db= new db_manager();
        $result = $db->query($sql);

        header("Location: ../userdetails.php?user=$user_id");
        
        
    }
    
    public function edit_thread($title, $category,$description,$thread_id){
        
        $sql = "UPDATE `threads` SET `category_ID`='$category',`title`='$title',`description`='$description' WHERE threads.thread_ID=$thread_id";
        
        $db= new db_manager();
        $result = $db->query($sql);

        
//        echo $title, $category, $description, $thread_id;
        header("Location: ../single_thread.php?thread_id=$thread_id");
        
        
    }
        
    public function upload_comment($user_id, $thread_id, $comment){
        
        $sql = "INSERT INTO `comments`(`user_id`, `date`, `thread_id`, `comments`) VALUES ('$user_id', NOW(), '$thread_id', '$comment')";
        
        $db = new db_manager();
        $result = $db->query($sql);
        
        $this->goBack();
        
    }
        
    public function update_vote($thread_id, $user_id){
        
        $insertSql = "INSERT INTO `votes`(`thread_id`, `user_id`) VALUES ('$thread_id','$user_id')";
        
        
        $db = new db_manager();
        //insert vote
        $result = $db->query($insertSql);
        
        
        //get the number of votes of the thread
        $votes = $db->get_number_of_votes_thread($thread_id);
        $current_votes = count($votes);
        
        
        //update the vote number to thread data
        $update_thread_votes = "UPDATE `threads` SET `votes`='$current_votes' WHERE thread_ID = $thread_id";
        
        $db->query($update_thread_votes);
        

        
        $this->goBack();
        
        
    }
        
        
    public function remove_thread($thread_id, $user_id){
        if(isset($_GET["remove_thread"]) && isset($_SERVER["HTTP_REFERER"])){
                        
            $sql = "DELETE FROM threads WHERE thread_ID = $thread_id";
            
            
            $db= new db_manager();
            $result = $db->query($sql);
                        
            $this->goBack();
        }
    }
        
    public function remove_comment($comment_id, $user_id){
        
        if(isset($_GET["remove_comment"]) && isset($_SERVER["HTTP_REFERER"])){

        $sql = "DELETE FROM comments WHERE comment_id = $comment_id";


        $db= new db_manager();
        $result = $db->query($sql);
        
        $this->goBack();
        }
    }
        
    public function remove_vote($thread_id, $user_id){
        
        $sql = "DELETE FROM votes WHERE user_id = '$user_id' AND thread_id = '$thread_id'";
        
        $db = new db_manager();
        $result = $db->query($sql);
        
        $votes = $db->get_number_of_votes_thread($thread_id);
        $current_votes = count($votes);
        
        
        //update the vote number to thread data
        $update_thread_votes = "UPDATE `threads` SET `votes`='$current_votes' WHERE thread_ID = $thread_id";
        
        $db->query($update_thread_votes);
        
        
    
        $this->goBack();
        
    }
        
        
    public function goBack(){
        $return = $_SERVER["HTTP_REFERER"];
        header("Location: $return");
    }
        
}

new User_manager();

?>
