<?php
ob_start();
class Password_manager {
    
    public function __construct(){}
    
    public function hashPW($pw){
        $hashedPW = password_hash($pw, PASSWORD_DEFAULT);
        return $hashedPW;
    }
    
    public function check_password($password, $password_hashed){
        $is_matched = password_verify($password, $password_hashed);
        return $is_matched;
        //If they match, true, if not, false;
    }
    
    
}



?>