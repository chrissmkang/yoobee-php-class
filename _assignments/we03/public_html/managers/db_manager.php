<?php
ob_start();
define("DB_SERVER", "127.0.0.1");
define("DB_USER", "root");
define("DB_PASSWORD", "");
define("DB_NAME", "chris_Doodle");

class db_manager {
    
    private $db;
    
    public function __construct (){
        $this->db = new mysqli(DB_SERVER, DB_USER,DB_PASSWORD,DB_NAME);
        
        if($this->db->connect_error) {
            die("db error");
        } else {
            echo "<!--DB connected-->";
        }
    }
    
    public function query($sql){
        $result = $this->db->query($sql);
        return $result;
    }
    
    public function register_user($user_name, $first_name, $last_name, $password, $email, $avatar){
        //REGISTER USER TO DB
        
        $result = false;
        
        $sql = "INSERT INTO `users`(`user_name`, `first_name`, `last_name`, `password`, `email`, `avatar`, `privilege`) VALUES ('$user_name', '$first_name', '$last_name', '$password', '$email', '$avatar', '0')";
                
        $result = $this->db->query($sql);
        return $result;
        //returns true when user has been created.
        //if unique ID (user_name or email) exists, returns false.
    }
    
    public function update_user($user_id, $user_name, $first_name, $last_name, $email, $avatar){
        //Edit USER TO DB
        $result = false;
                
        $sql= "UPDATE `users` SET `user_name`='$user_name',`first_name`='$first_name',`last_name`='$last_name',`email`='$email',`avatar`='$avatar',`privilege`='0' WHERE users.user_id = '$user_id'";
        
        $result = $this->db->query($sql);
        
        return $result;
        //returns true when user has been updated.
        //if unique ID (user_name or email) exists, returns false.
    }
    
    
    
    
//    ***********-----------GET FUNCTIONS BELOW-------------**************
    
    public function get_user_detail($user_name){
        //GET SINGLE USER DETAILS BY USER NAME
        $sql = "SELECT * FROM users WHERE users.user_name = '$user_name'";
        $result = $this->db->query($sql);
        $user_detail = [];
        
        if($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            array_push($user_detail, $row);
        }
        return $user_detail;
    }
    
    public function get_user_by_id($user_id){
        //GET SINGLE USER DETAILS BY ID
        $sql = "SELECT * FROM users WHERE users.user_id = '$user_id'";
        $result = $this->db->query($sql);
        $user = [];
        
        if($result->num_rows > 0) {
            while($row = $result->fetch_assoc()){
                array_push($user,$row);
            }
        }
        return $user;
    }
    
    public function get_thread_by_id($thread_ID){
        $sql = "SELECT * FROM threads WHERE thread_ID = '$thread_ID'";
        $result = $this->db->query($sql);
        $thread = [];
        
        if($result->num_rows > 0 ){
            $row = $result->fetch_assoc();
            array_push($thread, $row);
        }
        return $thread;
    }
    
    public function get_thread_by_category_id($category_ID){
        $sql = "SELECT * FROM threads WHERE category_ID = '$category_ID'";
        $result = $this->db->query($sql);
        $thread = [];
        
        if($result->num_rows > 0 ){
            while($row = $result->fetch_assoc()){
                array_push($thread, $row);
            }
        }
        return $thread;
    }
    
    public function get_user_threads($user_page_id){
        $sql = "SELECT * FROM threads WHERE user_id = '$user_page_id'";
        $result = $this->db->query($sql);
        $threads = [];
        
        if($result->num_rows >0 ){
            while($row = $result->fetch_assoc()){
                array_push($threads, $row);
            }
        }
        return $threads;
    }
    
    public function get_password($user_name){
        //FUNCTION TO GET A PASSWORD OF THIS USER
        
        $stored_password = "";
        $getMatchingNameSQL = "SELECT * FROM users WHERE users.user_name = '$user_name'";
        
        $result = $this->db->query($getMatchingNameSQL);
        
        if($result->num_rows > 0 ){
            $row = $result->fetch_assoc();
            $stored_password = $row["password"];
        }
        return $stored_password;
    }
    
    public function get_current_user($session_user_name){
        //WHO IS LOGGED IN?
        $sql = "SELECT * FROM users WHERE users.user_name = '$session_user_name'";
        $result = $this->db->query($sql);
        $current_user = [];
        
        if($result->num_rows == 1) {
            $row = $result->fetch_assoc();
            array_push($current_user, $row);
        }
        
        return $current_user;
    }
    
    public function get_all_users(){
        $sql ="SELECT * FROM users";
        $result = $this->db->query($sql);
        $users = [];
        
        if($result->num_rows > 0) {
           while($row = $result->fetch_assoc()){
            array_push($users,$row);   
           }
        }
        return $users;
    }
    
    public function get_all_categories() {
        $sql ="SELECT * FROM categories";
        $result = $this->db->query($sql);
        $categories = [];
        
        if($result->num_rows > 0) {
           while($row = $result->fetch_assoc()){
            array_push($categories,$row);   
           }
        }
        return $categories;
    } 
    
    public function get_enabled_categories() {
        $sql ="SELECT * FROM categories WHERE categories.status = 'true'";
        $result = $this->db->query($sql);
        $categories = [];
        
        if($result->num_rows > 0) {
           while($row = $result->fetch_assoc()){
            array_push($categories,$row);   
           }
        }
        return $categories;
    }
    
    public function get_all_threads() {
        $sql = "SELECT * FROM `threads` WHERE threads.status = 'true' ORDER BY votes DESC";
        $result = $this->db->query($sql);
        $threads = [];
        
        if($result->num_rows>0){
            while($row=$result->fetch_assoc()){
                array_push($threads, $row);
            }
        }
        return $threads;
    }
    
    public function get_comments_of_thread($thread_ID){
        $sql = "SELECT * FROM comments WHERE thread_id = '$thread_ID'";
        $result = $this->db->query($sql);
        $comments = [];
        
        if($result->num_rows>0){
            while($row=$result->fetch_assoc()){
                array_push($comments,$row);
            }
        }
        return $comments;
    }
    
    public function get_number_of_votes_thread($thread_id){
        $sql = "SELECT * FROM votes WHERE thread_id = '$thread_id'";
        
        $result = $this->db->query($sql);
        $voted = [];
        
        if($result->num_rows > 0 ){
            while($row=$result->fetch_assoc()){
                array_push($voted , $row);
            }
        }
        
        return $voted;
    }
    
}

?>
