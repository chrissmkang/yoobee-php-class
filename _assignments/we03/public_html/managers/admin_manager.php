<?php 
include_once("db_manager.php");
ob_start();

//$_GET["update_category_status"] == category ID #
    
    class Admin_Manager {
    
    public function __construct(){
        
        if (isset($_GET["update_category_status"]) && isset($_SERVER['HTTP_REFERER'])) {
            $this->toggle_category_status($_GET["update_category_status"]);
        }
        
        if(isset($_POST["category_add_submit"]) && isset($_SERVER["HTTP_REFERER"])){
            $this->add_category($_POST["category_name"]);
        }
        
        if(isset($_GET["remove_user"]) && isset($_SERVER["HTTP_REFERER"])){
            $this->remove_user($_GET["remove_user"]);
        }
        
        if(isset($_GET["remove_thread"]) && isset($_SERVER["HTTP_REFERER"])){
            $this->remove_thread($_GET["remove_thread"]);
        }
        
    }
    
    public function toggle_category_status($categoryID){
         if (isset($_GET["update_category_status"]) && isset($_SERVER['HTTP_REFERER'])) {
             $sql = "UPDATE categories SET status=(SELECT CASE status WHEN 'true' THEN 'false' ELSE 'true' END) WHERE category_id = $categoryID";
             
             $sql2 = "UPDATE threads SET status=(SELECT CASE status WHEN 'true' THEN 'false' ELSE 'true' END) WHERE category_ID = $categoryID";
             
             
             
             $db = new db_manager();
             $result = $db->query($sql);
             $result = $db->query($sql2);
             
             header("Location: ../admin_page.php");
         }
    }
        
    public function add_category($category_name){
        if(isset($_POST["category_add_submit"]) && isset($_SERVER["HTTP_REFERER"])){
            $sql = "INSERT INTO `categories` (`category_name`, `status`) VALUES ('$category_name','true')";
            
            $db = new db_manager();
            
            $result = $db->query($sql);
            
            header("Location: ../admin_page.php");
        }
    }
    public function remove_user($user_id){
        if(isset($_GET["remove_user"]) && isset($_SERVER["HTTP_REFERER"])){
            $sql = "DELETE FROM users WHERE users.user_id= $user_id";
            
            $sql_two = "DELETE FROM threads WHERE user_id = $user_id";
            
            
            $db= new db_manager();
            $result = $db->query($sql);
            $result = $db->query($sql_two);
            
            header("Location: ../admin_page.php");
        }
    }
    
    public function remove_thread($thread_id){
        if(isset($_GET["remove_thread"]) && isset($_SERVER["HTTP_REFERER"])){
                        
            $sql = "DELETE FROM threads WHERE thread_ID = $thread_id";
            
            
            $db= new db_manager();
            $result = $db->query($sql);
                        
            header("Location: ../admin_page.php");
        }
    }
        
}

new Admin_manager();

?>
