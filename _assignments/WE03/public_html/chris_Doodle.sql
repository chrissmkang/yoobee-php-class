-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 15, 2017 at 10:39 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chris_Doodle`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` tinyint(4) NOT NULL,
  `category_name` varchar(200) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `status`) VALUES
(1, 'Doodle', 'true'),
(2, 'Digital Paint', 'true'),
(3, 'Manga', 'true'),
(4, 'Concept Sketch', 'true'),
(5, 'Illustration', 'true'),
(6, 'Tutorial', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `comment_id` tinyint(4) NOT NULL,
  `user_id` tinyint(4) NOT NULL,
  `date` datetime NOT NULL,
  `thread_id` tinyint(4) NOT NULL,
  `comments` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`comment_id`, `user_id`, `date`, `thread_id`, `comments`) VALUES
(1, 6, '2016-12-19 12:12:31', 10, '!!!'),
(2, 6, '2016-12-19 13:25:00', 10, 'This is awesome!'),
(5, 2, '2016-12-19 13:46:19', 10, 'wow!'),
(6, 2, '2016-12-22 20:13:22', 1, 'Please everyone vote this post!!'),
(7, 5, '2016-12-22 20:18:22', 5, 'Awesome!'),
(8, 5, '2016-12-22 20:18:35', 4, 'haha'),
(9, 5, '2016-12-22 20:18:49', 6, 'Looks delicious! :D'),
(10, 6, '2017-01-05 14:46:06', 6, 'awesome!'),
(11, 3, '2017-01-11 12:17:51', 2, 'This is brilliant!'),
(12, 3, '2017-01-11 12:17:59', 9, 'Love it!'),
(13, 3, '2017-01-11 12:18:07', 6, 'Thank you guys~'),
(14, 3, '2017-01-11 12:18:23', 15, 'Cool!'),
(15, 3, '2017-01-11 12:18:39', 10, 'This is amazing!'),
(16, 2, '2017-01-11 12:19:01', 3, 'Nice!'),
(17, 2, '2017-01-11 12:19:15', 14, 'Did you draw this or is it a screenshot?'),
(18, 2, '2017-01-11 12:19:30', 16, 'Good depth of field!'),
(19, 2, '2017-01-11 12:19:38', 7, 'Niceeee'),
(20, 2, '2017-01-11 12:19:58', 17, 'I vote for this!! Go Go'),
(21, 3, '2017-01-11 14:14:52', 24, 'Wow thank you~'),
(22, 5, '2017-01-16 08:19:09', 10, 'Thanks guys'),
(23, 6, '2017-01-16 08:21:09', 25, 'Great job'),
(24, 6, '2017-01-16 08:57:39', 12, 'Heidi thats good work!');

-- --------------------------------------------------------

--
-- Table structure for table `threads`
--

CREATE TABLE `threads` (
  `thread_ID` mediumint(9) NOT NULL,
  `category_ID` tinyint(4) NOT NULL,
  `title` varchar(100) NOT NULL,
  `image` varchar(200) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `user_ID` tinyint(4) NOT NULL,
  `status` varchar(10) NOT NULL,
  `votes` mediumint(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `threads`
--

INSERT INTO `threads` (`thread_ID`, `category_ID`, `title`, `image`, `description`, `user_ID`, `status`, `votes`) VALUES
(1, 1, 'Snowy Town', '2_2_6a17762e3d7b1435706fde9301c76502.jpg', 'This is an image I came across when I saw a snow storm outside today', 2, 'true', 2),
(2, 5, 'Final project', '5_2_8f723a9a5885594f866874dbca3591b5.jpg', 'Hi all,\r\n\r\nI happen to finish this project for my gallery art show project.\r\n\r\nFeel free to vote me up!!\r\n\r\nPEACE!', 5, 'true', 4),
(3, 2, 'Good day', '4_2be4c25b2c1df622c65df758ab7c4122.jpg', 'No description', 4, 'true', 2),
(4, 4, 'Hmmmm', '4_3e9e61f18d9282469bd45d7d2b678aa3.jpg', 'This is sometimes what I do..\r\n\r\n\r\nHMMMmm...', 4, 'true', 4),
(5, 4, 'Cowboy', '6_9c5fdc37e9a0e297c586c9b84fd1f36a.jpg', 'Bang bang bangggg!!', 6, 'true', 3),
(6, 5, 'Hamburger', '3_19dc3d3f392a57378c55c32e3ef7574e.jpg', 'Yum!', 3, 'true', 5),
(7, 5, 'Digital Illustration!', '3_35bb5e57db0be6bf089e59be91bea099.jpg', 'Hi guys~\r\n\r\nplease vote my work!!', 3, 'true', 3),
(8, 1, 'Just a quick sketch', '7_35cbe0c40b4e23ee83d8295ab02cbacf.jpg', 'thank you everyone!', 7, 'true', 3),
(9, 5, 'Please vote this image!!', '2_41fae0ec20d0cf6adeaefdb67810a0c7.jpg', 'Hi guys!!\r\n\r\n\r\nPlease vote this image!!\r\nThank you so much!', 2, 'true', 4),
(10, 2, '30min paint', '5_51eb7538ada08de3d42cbef209d05d81.jpg', 'Just a quick sketch', 5, 'true', 2),
(11, 1, '10 min doodle', '5_277bc2d3106fb56f4e46abbc2dba9eaa.jpg', 'Quck sketch', 5, 'true', 2),
(12, 3, 'A blue haired girl', '3_304a4ab832136eda3f39537c76318884.jpg', 'What do you guys think?\r\n\r\nIf you like this image!! please vote for this!!', 3, 'true', 6),
(13, 5, 'Two couples', '7_1051f3f50c306fffbefeaa4e43cd516b.jpg', ':D', 7, 'true', 3),
(14, 3, 'Doodling', '5_Zen_and_Serena_by_TheBlackRoseWitch456.png', 'Just had time to doodle', 5, 'true', 2),
(15, 1, 'Series of backgrounds', '5_1451c48f4cd84e43dac27c27f8e36b8a.jpg', 'I hope you guys like it!', 5, 'true', 3),
(16, 1, '5 min doodling!', '5_5357842d632b26e0b4f762cd830a5b12.jpg', 'Just a doodle!', 5, 'true', 2),
(17, 5, 'Lion the Lion', '3_d6fc527cf7b8000cc45e97fc04f0dcec.jpg', 'Hi guys,\r\n\r\nJust finished my final design for Lionnnn\r\n\r\n\r\nThank you', 3, 'true', 3),
(18, 4, 'Concept Sketch', '4_69209dc08a7254c2732f48b070e7c1c8.jpg', 'Hi All,\r\n\r\nHope you all like it', 4, 'true', 3),
(19, 4, 'Womarrior', '4_bcae31b362c9aa4241c4a3dcd71175bf.jpg', 'hah!', 4, 'true', 3),
(20, 3, 'Cutey Girl', '7_P-Cola_by_Kuvshinov-Ilya.jpg', 'haha', 7, 'true', 1),
(21, 5, 'Girl with sunglasses', '6_a9e818115a12682a2a59ad3812376b94.jpg', 'It took a few hours to finish this but it&#39;s worth it!', 6, 'true', 2),
(22, 2, 'Speed painting', '4_Speed_paint_by_ZurdoM.jpg', 'I enjoyed it\r\nHope you enjoy it also', 4, 'true', 2),
(23, 6, 'Just a quick tip on eyes', '6_How-to-Draw-an-EYE-8.jpg', 'This is how I usually draw eyes for real-life drawing.\r\n\r\nHope to see how others draw too!', 6, 'true', 2),
(24, 6, 'How to draw heads in different perspective', '6_how_to_draw_the_human_head_10.jpg', 'Just keep practicing!', 6, 'true', 2),
(25, 3, 'I love this girl', '5_Chitanda_Eru_by_TempestDH.png', 'YAY\r\n\r\nThis is my new painting i did over the holiday', 5, 'true', 1),
(26, 3, 'Cute Girl', '4_mirai_kuriyama_by_nebwei.png', 'This is a new cute girl i drew', 4, 'true', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` tinyint(4) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `password` varchar(150) NOT NULL,
  `email` varchar(200) NOT NULL,
  `avatar` varchar(300) NOT NULL,
  `privilege` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `first_name`, `last_name`, `password`, `email`, `avatar`, `privilege`) VALUES
(1, 'admin', 'Admin', ':)', '$2y$10$cvIM/TPx5jMbhHpYfUK9ie2D6UkWJqiAuuiM5.cVuriYHzkJgRm.m', 'admin@doodle.com', 'default.jpg', 1),
(2, 'chris542', 'Chris', 'Kang', '$2y$10$PuBY6VWjB5lu88Pl6JS6cu9m/TT2sFxl/i7iJ6qfVOqv3KwdUO9ku', 'chris.sm.kang542@gmail.com', 'chris.jpg', 0),
(3, 'heidi', 'Heidi', 'Duxfield', '$2y$10$O61lXEdwPtKBWD4qsN.bqOTYRgptGWqyl64zGAnm98PlEIlXI92LG', 'heidi@gmail.com', 'heids.jpg', 0),
(4, 'kevin', 'Kevin', 'Dowd', '$2y$10$pyoI8vVTSKUWG9ZimNfASuhikj0OQ1lEAKzJkvNnNQYGnr84lccV.', 'kevin@gmail.com', 'default.jpg', 0),
(5, 'robert', 'Robert', 'La', '$2y$10$l72W0z.iQOab.i1fqMJeC.dgjYUgctYILnq0zFOm613sbdsyuO0KC', 'robby@gmail.com', 'robert.jpg', 0),
(6, 'chris', 'Chris', 'Kang', '$2y$10$fjATJCb48dEVFHE2BBUBKOjkpJdt0OAcCLDSOg.6/Mfn1R2WsrYS6', 'chris542@hotmail.com', '6á„‹á…¯á†¯ 12á„‹á…µá†¯ (5).jpg', 0),
(7, 'bono', 'Bono', 'Kim', '$2y$10$w...28p7E4Wxf9SBw5MjguWSuRSpJLVWi1GRbv1nSs5R1TRnvQ63i', 'bonokim@gmail.com', 'default.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `votes`
--

CREATE TABLE `votes` (
  `thread_id` tinyint(4) NOT NULL,
  `user_id` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `votes`
--

INSERT INTO `votes` (`thread_id`, `user_id`) VALUES
(2, 5),
(9, 2),
(11, 3),
(9, 3),
(5, 3),
(2, 3),
(6, 3),
(12, 3),
(13, 7),
(12, 6),
(10, 6),
(0, 2),
(4, 2),
(6, 2),
(5, 2),
(12, 2),
(12, 5),
(9, 5),
(4, 5),
(6, 5),
(8, 5),
(5, 4),
(4, 4),
(1, 4),
(3, 4),
(6, 4),
(2, 4),
(14, 5),
(15, 3),
(13, 3),
(16, 3),
(4, 3),
(8, 3),
(7, 3),
(17, 3),
(10, 3),
(16, 2),
(7, 2),
(15, 2),
(17, 2),
(18, 4),
(19, 4),
(13, 4),
(17, 4),
(9, 6),
(11, 6),
(18, 6),
(15, 6),
(20, 7),
(8, 7),
(14, 7),
(2, 7),
(19, 7),
(12, 7),
(21, 6),
(21, 4),
(7, 4),
(12, 4),
(22, 4),
(23, 6),
(24, 6),
(24, 3),
(22, 3),
(1, 3),
(19, 3),
(23, 3),
(18, 3),
(3, 3),
(25, 6),
(6, 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `threads`
--
ALTER TABLE `threads`
  ADD PRIMARY KEY (`thread_ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_name` (`user_name`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `comment_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `threads`
--
ALTER TABLE `threads`
  MODIFY `thread_ID` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
