<?php 
include("managers/db_manager.php");
include("includes/header.php");

$db_manager = new db_manager();
$categories = $db_manager->get_all_categories();

$thread = $db->get_thread_by_id($_GET["edit_thread"]);



?>

    <div id="edit_thread">

        <div class="container">
            <h1>Edit Thread</h1>
            <div class="row">
               <div class="col-12 img_box">
                    <img id="thread_image" src="asset/threads/<?= $thread[0]["image"]; ?>" alt="">
                </div>
                
                
            <form action="scripts/process_thread.php" method="post" enctype="multipart/form-data">

                <fieldset class="row">
                    <label class="col-2" for="title">Title</label>
                    <input type="hidden" name="thread_id" value="<?= $thread[0]["thread_ID"]; ?>">
                    <input class="col-8" type="hidden" name="title_original" value="<?= $thread[0]["title"]; ?>">
                    <input class="col-8" type="text" name="title" value="<?= $thread[0]["title"]; ?>">
                </fieldset>


                <fieldset class="row">
                    <label class="col-2" for="category">Category</label>
                    <select class="col-8" name="category">
                        <?php foreach($categories as $category) : ?>

                            <?php if($category["category_id"] == $thread[0]["category_ID"]) : ?>

                                <option value="<?= $category["category_id"]; ?>" selected>
                                    <?= $category["category_name"]; ?>
                                </option>

                                <?php else : ?>

                                    <option value="<?= $category["category_id"]; ?>">
                                        <?= $category["category_name"]; ?>
                                    </option>

                                    <?php endif; ?>

                                        <?php endforeach; ?>
                    </select>
                </fieldset>

                <fieldset class="row">
                    <label class="col-2" for="description">Description</label>
                    <textarea name="description" id="" cols="30" rows="10" class="col-8"><?= $thread[0]["description"]; ?></textarea>
                </fieldset>


                <fieldset class="row">
                    <div class="col-2"></div>
                    <div class="col-8">
                        <button type="submit" name="edit_thread">Edit</button>
                    </div>
                </fieldset>


            </form>
        </div>




    </div>


    <?php 
include("includes/footer.php");
?>
