<?php 

    class Upload {
        
        public $fileName;
        public $fileTmp;
        
        public function __construct($fileName, $fileTmp){
            $this->fileName = $fileName;
            $this->fileTmp = $fileTmp;
        }
        
        public function existCheck() {
            $fileNameArray = explode(".", $this->fileName);
            $fileExtension = end($fileNameArray);
            
            if(file_exists("uploaded/".$this->fileName)){
                $this->fileName = $fileNameArray[0]. rand(0,9999) . "." . $fileExtension;
            }
        }
        
        public function main (){
            
            $this->existCheck();
            
            if (move_uploaded_file($this->fileTmp, "uploaded/".$this->fileName) ) {
            //move_uploaded_file = in built php function

                //$imgFullpath = $_SERVER['HTTP_ORIGIN'] . dirname($_SERVER["REQUEST_URI"] . '?') . $uniqueFileName;
                $imgFullpath =  "uploaded/".$this->fileName;
                echo "<img src='$imgFullpath' alt='image_preview'/><br>";
                echo "<span>Your File Uploaded Succesfully...!!</span><br>";
                echo "<br><b>File Name:</b> " . $_FILES["file"]["name"] . "<br>";
                echo "<b>Type:</b> " . $_FILES["file"]["type"] . "<br>";
                echo "<b>Size:</b> " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
                echo "<b>Temp file:</b> " . $_FILES["file"]["tmp_name"] . "<br>";
            }
        }
        
       
    
    }

?>
