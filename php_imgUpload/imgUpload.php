<!DOCTYPE html>
<html>

<head>
    <title>Upload Image using form</title>
    <link href="css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="forms-min.css">
    <style>
        * {
            font-family: helvetica;
        }
        
        .code-block {
            background-color: #333;
            color: #fff;
            padding: 1em;
        }
        
        code,
        code * {
            font-family: monospace;
        }
        
        .imp {
            color: yellow;
        }
        
        i {
            color: dodgerblue;
        }

    </style>

</head>

<body>
    <div id="mainform">
        <div id="innerdiv">
            <h2>Upload Image with form</h2>
            <!-- Required Div Starts Here -->

            <h4>Making form to upload an image (HTML)</h4>
            <p>&lt;form action="" enctype="multipart/form-data" id="form" method="post" name="form"></p>
            <p>&lt;input id="file" name="file" type="file"></p>
            <p>&lt;input name="img-submit" type="submit" value="Upload"></p>


            <div id="formdiv">

                <form action="" enctype="multipart/form-data" id="form" method="post" name="form" class="pure-form pure-form-stacked">
                    <div id="upload">
                        <input id="file" name="file" type="file">
                    </div>

                    <input name="img-submit" type="submit" value="Upload">
                </form>

                <div id="detail">
                    <b>Note:</b>
                    <ul>
                        <li>To Choose file Click on folder.</li>
                        <li>You can upload- <b>images (jpeg, jpg, png).</b></li>
                        <li>Image should be less than 100kb in size.</li>
                    </ul>
                </div>
            </div>

            <ul>
               <li>
                   <p>To check file input array use :</p>
                   <div class="code-block"><code>
                       <span class="imp">echo</span> '&lt;pre>';<br>
                       <span class="imp">print_r</span>(<span class="imp">$_FILES</span>);<br>
                       <span class="imp">echo</span> '&lt;/pre>';
                       
                   </code></div>
               </li>
                <li>
                    <p>When the submit button has been clicked, and if there are any errors, show this message.</p>
                    <div class="code-block"><code>
                <span class="imp">if</span> (<span class="imp">isset</span>($_POST['img-submit'])) {<br>
                <br>
                <span class="imp">if</span>($_FILES["file"]["error"] > 0) {<br>
                &nbsp;<span class="imp">echo</span> "Return Code: " . $_FILES['file']['error'] . ";<br>
                &nbsp;<span class="imp">exit</span>;<br>
                }<br>
                <br>}
            </code></div>
                </li>
                <li>
                    <p>Checks the file extension</p>
                    <div class="code-block"><code><i>$validExtensions</i> =    array ("jpeg",'jpg','png');<br>
                          <i>$temporary</i> = <span class="imp">explode</span>(".", $_FILES['file']['name']);<br>
                        <i>$file_extension</i> = <span class="imp">end</span>($temporary);<br><br>
                              <span class="imp">if</span>(<span class="imp">in_array</span>(<i>$file_extension</i>, <i>$validExtensions</i>) === <span class="imp">false</span>) {<br>
                            &nbsp;<span class="imp">die</span>("not a valid filetype");<br>
                                 }
                               </code></div>
                </li>
                <li>
                    <p>Store file name and temp_file name in a variable</p>
                    <div class="code-block"><code>
                        <i>$file_name</i> = $_FILES['file']['name'];<br>
                        <i>$file_tmp</i> = $FILES['file']['tmp_name'];</code></div>

                </li>
                <li>
                    <p>Instansiate a class called Upload</p>
                    <p>Pass through two attributes, $file_name and $file_tmp</p>
                    <div class="code-block"><code>
                        <i>$uploadClass</i> = <span class="imp">new</span> <span class="imp">Upload</span>(<i>$file_name</i>, <i>$file_tmp</i>);
                    </code></div>
                </li>
                <li>
                    <p>Make a Class Upload</p>
                    <p>In Upload, we pass two variables in __construct(), which will assign two attributes from outside of Class</p>
                    <p>Make two methods called existCheck() and main()</p>
                    <div class="code-block"><code>
                    <span class="imp">class</span> <i>Upload</i> {<br>
                    <br>
                    &nbsp;<span class="imp">public</span> <i>$fileName</i>;<br>
                    &nbsp;<span class="imp">public</span> <i>fileTmp</i>;<br>
                    <br>
                    &nbsp;<span class="imp">public function</span> <i>__construct</i>($fileName, $fileTmp){<br>
                    &nbsp;&nbsp;$this->fileName = $fileName;<br>
                    &nbsp;&nbsp;$this->fileTmp = $fileTmp;}        
                    <br>}
                </code></div>
                </li>
                <li>
                    <p>In existCheck(), we break down file name, then check if it exists. If so, add random number</p>
                    <div class="code-block"><code>
                        <span class="imp">public function</span> <i>existCheck()</i> {<br>
                        &nbsp;<i>$fileNameArray</i> = <span class="imp">explode</span>(".", $this->fileName);<br>
                        &nbsp;<i>$fileExtension</i> = end(<i>$fileNameArray</i>);<br>
                        &nbsp;<i>$tempFile</i> = $this-><i>fileTmp</i>;
                        <br><br>
                        &nbsp;<span class="imp">if</span>(file_exists("uploaded/".$this-><i>fileName</i>)){<br>
                        &nbsp;&nbsp;<i>$fileNameArray</i> = explode(".", $this-><i>fileName</i>);<br>
                        &nbsp;&nbsp;<i>$fileExtension</i> = end(<i>$fileNameArray</i>);<br>
                        <br>
                        &nbsp;&nbsp;<span class="imp">if</span>(<span class="imp">file_exists</span>("uploaded/".$this-><i>fileName</i>)){<br>
                        &nbsp;&nbsp;&nbsp;$this-><i>fileName</i> = $<i>fileNameArray</i>[0]. rand(0,9999) . "." . <i>$fileExtension</i>;<br>
                        &nbsp;&nbsp;}
                        <br>&nbsp;}
                        <br>
                        <br>}
                    </code></div>
                </li>
                <li>
                    <p>In main method, run existCheck() and then upload the file</p>
                    <p></p>
                    <div class="code-block"><code>
                               <span class="imp">public function</span> <i>main</i>() {<br>
                               &nbsp;$this-><i>existCheck()</i>;<br><br>
                               &nbsp;<span class="imp">if</span> (<span class="imp">move_uploaded_file</span>($this-><i>$file_tmp</i>, "uploaded/'.$this-><i>file_name</i>)) {<br>
                                &nbsp;&nbsp;<i>$imgPath</i> = "uploaded/".$this-><i>file_name</i>;<br>
                                &nbsp;&nbsp;<span class="imp">echo</span> "&lt;img src='<i>$imgPath'</i> alt='image_preview'/>&lt;br>';<br>
                                &nbsp;}<br>
                                <br>}

               </code></div>
                </li>
            </ul>

            <div class="showfile">
                <?php include "upload.php"; ?>
            </div>

        </div>
    </div>
</body>

</html>
