<?php
session_start();

if ( isset( $_POST["submit-login"]) ) {
    updateSession();
    returntoLoginPage();
} else if (isset ($_POST["submit-logout"])) {
    echo "LOG OUT";
    killSession();
    returntoLoginPage();
}


function updateSession(){
    $_SESSION["loggedin"] = TRUE;
    $_SESSION["username"] = $_POST["username-field"];
}

function killSession() {
    $_SESSION = [];
    session_unset();
}

function returntoLoginPage(){
//   echo "<pre>";
//   var_dump($_SERVER);
//   echo "</pre>";
  header("Location: {$_SERVER['HTTP_REFERER']}");
}


?>