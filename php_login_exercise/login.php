<?php 
    session_start();

    $islogged = false;

    if(isset($_SESSION['loggedin'])){
        echo 'user is logged in';
        $islogged = true;
    } else {
        echo 'user is not logged in';
    }

?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <title>Log In Page</title>
        <link href="style.css" rel="stylesheet" type="text/css">
    </head>

    <body>
        <ul class="menu">
            <li><a href="home.php">Home</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="contacts.php">Contacts</a></li>
            <li><a href="login.php">Log In</a></li>
        </ul>

        <?php if ( isset($_SESSION["loggedin"]) ): ?>


            <form class="my-form" action="scripts/process_login.php" method="post">
                <fieldset>
                    <legend>Log out:</legend>
                    <label for="username-field">Username: <span class="red-text"><?= $_SESSION["username"]; ?></span></label>
                    <input type="submit" name="submit-logout" value="Logout">
                </fieldset>
            </form>

            <?php else: ?>

                <form class="my-form" action="scripts/process_login.php" method="post">
                    <fieldset>
                        <legend>Log in here:</legend>

                        <label for="username-field">Username:</label>
                        <input type="text" value="" name="username-field" maxlength="50">

                        <label for="password-field">Password:</label>
                        <input type="password" value="" name="password-field" maxlength="30">


                        <input type="submit" name="submit-login" value="Login">
                    </fieldset>
                </form>


                <?php endif; ?>

                    <ol>
                        <li>
                            <p>The form is made out of :</p>
                            <div class="code-block"><code>
&lt;php <span class="imp">if</span> ( isset(<i>$_SESSION[<span class="imp">"loggedin"</span>]</i>)) ): ?><br>
<br>
&lt;form class="my-form" <i>action</i>="<span class="imp">scripts/process_login.php</span>" <i>method</i>="post"><br><br>
&nbsp;&lt;fieldset><br><br>
&nbsp;&nbsp;&lt;legend><span class="comment">Log out:</span>&lt;/legend><br><br>
&nbsp;&nbsp;&lt;label <i>for</i>="username-field"><span class="comment">Username:</span><br>
&nbsp;&nbsp;&nbsp;&lt;span class="red-text">&lt;?= $_SESSION["<span class="imp">username</span>"]; ?>&lt;/span<br>
&nbsp;&nbsp;&lt;/label><br><br>
&nbsp;&nbsp;&lt;input <i>type</i>="submit" <i>name</i>="<span class="HL">submit-logout</span>" <i>value</i>="<span class="comment">Logout</span>"><br><br>
&nbsp;&lt;/fieldset><br>
&lt;/form><br>
<br>
&lt;?php <span class="imp">else</span>:?><br>
<br>
&lt;form class="my-form" <i>action</i>="<span class="imp">scripts/process_login.php</span>" <i>method</i>="post"><br>
&lt;fieldset><br>
&nbsp;&lt;legend><span class="comment">Log in here</span>:&lt;/legend><br>
&nbsp;&nbsp;&lt;label <i>for</i>="<span class="imp">username-field</span>"><span class="comment">Username:</span>&lt;/label><br>
&nbsp;&nbsp;&lt;input <i>type</i>="text" <i>value</i>="" <i>name</i>="<span class="imp">username-field</span>" <i>maxlength</i>="50"><br>
&nbsp;&nbsp;&lt;label <i>for</i>="<span class="imp">password-field</span>"><span class="comment">Password:</span>&lt;/label><br>
&nbsp;&nbsp;&lt;input <i>type</i>="password" <i>value</i>="" <i>name</i>="<span class="imp">password-field</span>" <i>maxlength</i>="30"><br>
&nbsp;&nbsp;&lt;input <i>type</i>="submit" <i>name</i>="<span class="HL">submit-login</span>" <i>value</i>="<span class="comment">Login</span>"><br>
&lt;/fieldset><br>
&lt;/form><br><br>
&lt;?php <span class="imp">endif</span>; ?>
            </code></div>
                        </li>
                        <li>
                            <p>In process_login.php :</p>
                            <div class="code-block"><code>
&lt;?php<br>
<i>session_start</i>();<br>
<br>
<span class="imp">if</span> ( <span class="imp">isset</span>( <i>$_POST[<span class="imp">"submit-login"</span>]</i>) ) {<br>
&nbsp;<i>updateSession</i>();<br>
&nbsp;<i>returntoLoginPage</i>();<br>
}<br>
<br>

<span class="imp">function</span> <i>updateSession</i>(){<br>
&nbsp;$_SESSION["<i>loggedin</i>"] = true;<br>
&nbsp;$_SESSION["<i>username</i>"] = <i>$_POST[<span class="imp">"username-field"</span>]</i>;<br>
}<br>

<span class="imp">function</span> <i>returntoLoginPage</i>(){<br>
<span class="comment">&nbsp;//echo "&lt;pre>";<br>
&nbsp;//var_dump($_SERVER);<br>
&nbsp;//echo "&lt;/pre>";<br></span>
&nbsp;<span class="imp">header</span>(<span class="HL">"Location: {$_SERVER[<span class="imp">'HTTP_REFERER'</span>]}"</span>);<br>
}<br>
<br>

?>
               
               
                </code></div>
                        </li>
                        <li>
                            <p>Additionally, add these methods for log out:</p>
                            <div class="code-block"><code>
<span class="imp">else if </span>(<span class="imp">isset</span> (<i>$_POST[<span class="imp">"submit-logout"</span>]</i>)) {<br>
&nbsp;<i>killSession</i>();<br>
&nbsp;<i>returntoLoginPage</i>();<br>
}<br>
<br>
<span class="imp">function</span> <i>killSession</i>() {<br>
&nbsp;$_SESSION = [];<br>
&nbsp;<i>session_unset</i>();<br>
}
                            </code></div>
                        </li>
                        <li>
                            <p>Organize them into Classes</p>
                            <div class="code-block"><code>
                                <span class="imp">class</span> <i>Logout</i> {}
                            </code></div>
                            <div class="code-block"><code>
                                <span class="imp">class</span> <i>Login</i> {}
                            </code></div>
                            <p>Then call the class from if isset :</p>
                            <div class="code-block"><code>
                                <span class="imp">if</span> ( <span class="imp">isset</span> (<i>$_POST[<span class="imp">"submit-login"</span>]</i>)){<br>
                                &nbsp;$myLogIn = <span class="imp">new</span> <i>Login</i>();}<br>
                                &nbsp;$myLogIn = <span class="imp">new</span> <i>Logout</i>();}<br>
                                }
                            </code></div>
                        </li>
                    </ol>



    </body>

    </html>
