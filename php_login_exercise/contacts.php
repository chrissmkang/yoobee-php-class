<?php 
    session_start();

    $islogged = false;

    if(isset($_SESSION['loggedin'])){
        echo 'user is logged in';
        $islogged = true;
    } else {
        echo 'user is not logged in';
    }

?>


    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>Home</title>
        <link rel="stylesheet" href="style.css" type="text/css">
    </head>

    <body>
        <ul class="menu">
            <li><a href="home.php">Home</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="contacts.php">Contacts</a></li>
            <li><a href="login.php">Log In</a></li>
        </ul>

        <header>
            <h2>Welcome to Learning $_SESSION</h2>
            <ol>
                <li>
                    <p>Write session_start(); at the top.</p>
                    <div class="code-block"><code>
                        <i>session_start</i>();
                    </code></div>
                </li>
                <li>
                    <p>State $islogged to false.</p>
                    <div class="code-block"><code>
                        <i>$islogged</i> = false;
                    </code></div>
                </li>
                <li>Write an if statement :
                    <div class="code-block"><code>
                    <span class="imp">if</span>(<span class="imp">isset</span>($_SESSION['<i>loggedin</i>'])){<br>
                    <span class="comment">&nbsp;echo 'user is logged in';<br></span>
                    &nbsp;<i>$islogged</i> = true;<br>
                    } <span class="imp">else</span> {<br>
                    <span class="comment">&nbsp;echo 'user is not logged in';</span><br>
                    }
                </code></div>
                </li>
            </ol>
        </header>
        <div class="status">
            <?php if ($islogged): ?>
                <div>
                    <h3>Yes, I am logged in as <?php echo $_SESSION["username"];?></h3>
                </div>
                <?php else: ?>
                    <div>
                        <h3>No, I am logged out</h3>
                    </div>
                    <?php endif; ?>

                  <?php if ($islogged == true) : ?>
                            <div class="logInBox"><a href="login.php" target="_self">log-out</a></div>
                            <?php else: ?>
                                <div class="logInBox"><a href="login.php" target="_self">log-in</a></div>
                                <?php endif; ?>
        </div>
                   
    </body>

    </html>
