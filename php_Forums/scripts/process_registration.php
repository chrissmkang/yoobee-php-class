<?php
include ("../functions/utils.php");
include ("../includes/db_connection.php");
include ("../includes/password_manager.php");

if(isset($_POST["submit-registration"])){
    
    $userName = $_POST["userName"];
    $userName = filter_var($userName, FILTER_SANITIZE_STRING);
    
    $firstName = $_POST["firstName"];
    $firstName = filter_var($firstName, FILTER_SANITIZE_STRING);
    
    $lastName = $_POST["lastName"];
    $lastName = filter_var($lastName, FILTER_SANITIZE_STRING);
    
    $email = $_POST["email"];
    $email = filter_var($email, FILTER_SANITIZE_STRING);
    
    $password = $_POST["password"];
    $passwordManager = new PasswordManager();
    $password_hashed = $passwordManager->hash_password($password);
    
    
    $insertQuery = "INSERT INTO users VALUES (NULL,'$userName','$firstName','$lastName','$email','$password_hashed')";
    
    echo $insertQuery. '<br>';
        
    $db_connect = new Connect();
    $insertUser = $db_connect->query($insertQuery);
        
    $insertUserCompleted = ($insertUser)? '../registration_thanks.php?registration=true' : $_SERVER["HTTP_REFERER"]. '?registration=false';    
//    $meh = (condition)? "true" : "false";
    
    header("Location: $insertUserCompleted");
//    header("Location: takes to this page after the submit");
    
    dumpObject();
}

if(isset($_POST["truncate-registration"])){
    $db_connect = new Connect();
    $db_connect->truncate();
    
    header("Location: {$_SERVER['HTTP_REFERER']}");
//    {} used to to retrieve an array inside ""
}

?>
