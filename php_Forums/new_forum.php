<?php 
session_start();
include ("includes/header.php");
include ("includes/navigation.php");
include ("includes/db_connection.php");

if(isset($_POST["submit-forum"])){
    
    if(!empty($_POST["name"])){
        $sql = "INSERT INTO `forums` VALUES (NULL,'".$_POST['name']."');";
        
        $db = new Connect();
        $db->query($sql);
        
        header("Location: ./index.php");
        
    } else {
        echo "Please fill in the forum name before submit";
    }
}
?>

    <form action="<?php $_SERVER['PHP_SELF'];?>" method="post" class="pure-form">
        <fieldset>
            <legend>Create a new forum</legend>
            <input name="name" type="text" placeholder="Forum Name">
            <button name="submit-forum" type="submit" class="pure-button pure-button-primary">Submit</button>
        </fieldset>
    </form>

    <ol>
        <li>On this php file : </li>
        <div class="code-block"><code>
&lt;?php <br>
<span class="red">session_start</span>();<br>
<span class="red">include</span> ("includes/header.php");<br>
<span class="red">include</span> ("includes/navigation.php");<br>
<span class="red">include</span> ("includes/db_connection.php");<br>
<br>
<span class="red">if</span>(<span class="red">isset</span>($_POST["submit-forum"])){<br>
    <br>
&nbsp;<span class="red">if</span>(<span class="red">!empty</span>($_POST["name"])){<br>
&nbsp;&nbsp;<span class="blue">$sql</span> = "INSERT INTO `forums` VALUES (NULL,'".$_POST['name']."');";<br>
        <br>
&nbsp;&nbsp;<span class="blue">$db</span> = new Connect();<br>
&nbsp;&nbsp;$db->query(<span class="blue">$sql</span>);<br>
        <br>
&nbsp;&nbsp;<span class="red">header</span>("Location: ./index.php");<br>
        <br>
&nbsp;} <span class="red">else</span> {<br>
&nbsp;&nbsp;<span class="red">echo</span> "Please fill in the forum name before submit";<br>
&nbsp;}<br>
}<br><br>
?>
<br>
<br>
<br>
&lt;<span class="yellow">form</span> <span class="red">action</span>="&lt;?php $_SERVER['PHP_SELF'];?>" <span class="red">method</span>="post" <span class="red">class</span>="pure-form"><br>
&nbsp;&lt;<span class="yellow">fieldset</span>><br>
&nbsp;&nbsp;&lt;<span class="yellow">legend</span>><span class="comment">Create a new forum</span>&lt;/<span class="yellow">legend</span>><br>
&nbsp;&nbsp;&lt;<span class="yellow">input</span> <span class="red">name</span>="name" <span class="red">type</span>="text" <span class="red">placeholder</span>="Forum Name"><br>
&nbsp;&nbsp;&lt;<span class="yellow">button</span> <span class="red">name</span>="submit-forum" <span class="red">type</span>="submit" <span class="red">class</span>="pure-button pure-button-primary"><span class="comment">Submit</span>&lt;/<span class="yellow">button</span>><br>
&nbsp;&lt;/<span class="yellow">fieldset</span>><br>
&lt;/<span class="yellow">form</span>>
       
        </code></div>
    </ol>

    <?php 
include ("includes/footer.php");
?>
