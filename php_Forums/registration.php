<?php
$title = "Registeration";
include ("includes/header.php");
include ("includes/navigation.php");
?>
    <!--MAIN CONTENT-->



    <div class="register">
        <h2>Register</h2>

        <form action="scripts/process_registration.php" method="post" class="pure-form">
            <fieldset>
                <legend>Register Form:</legend>
                <div class="pure-control-group">
                    <input name="userName" type="text" placeholder="User Name">
                    <input name="firstName" type="text" placeholder="First Name">
                    <input name="lastName" type="text" placeholder="Last Name">
                    <input name="email" type="email" placeholder="Email">
                    <input name="password" type="password" placeholder="Password">
                    <br>
                    <button type="submit" name="submit-registration" class="pure-button pure-button-primary">Sign in</button>
                    <button type="submit" name="truncate-registration" class="pure-button pure-button-primary">DELETE ALL</button>
                </div>
            </fieldset>
        </form>
    </div>

    <ol>
        <li>Make a scripts folder -> process_registration.php</li>
        <li>IF the form was submitted, collect all variables.</li>
        <div class="code-block"><code>
<span class="red">if</span>(<span class="red">isset</span>($_POST["submit-registration"])){<br>
    
 &nbsp;<span class="blue">$userName</span> = $_POST["userName"];<br>
 &nbsp;<span class="blue">$userName</span> = <span class="red">filter_var</span>($userName, <span class="red">FILTER_SANITIZE_STRING</span>);<br>
    <br>
 &nbsp;<span class="blue">$firstName</span> = $_POST["firstName"];<br>
 &nbsp;<span class="blue">$firstName</span> = <span class="red">filter_var</span>($firstName, <span class="red">FILTER_SANITIZE_STRING</span>);<br>
    <br>
 &nbsp;<span class="blue">$lastName</span> = $_POST["lastName"];<br>
 &nbsp;<span class="blue">$lastName</span> = <span class="red">filter_var</span>($lastName, <span class="red">FILTER_SANITIZE_STRING</span>);<br>
    <br>
 &nbsp;<span class="blue">$email</span> = $_POST["email"];<br>
 &nbsp;<span class="blue">$email</span> = <span class="red">filter_var</span>($email, <span class="red">FILTER_SANITIZE_STRING</span>);<br>
    <br>
 &nbsp;<span class="blue">$password</span> = $_POST["password"];<br>
 <br>
    
    </code></div>
        <li>Make a password_manager.php, and create methods to hash password.</li>
        <div class="code-block"><code>
<span class="red">public function</span> hash_password(<span class="blue">$pw</span>) {<br>
&nbsp;<span class="blue">$password_hashed</span> = <span class="red">password_hash</span>(<span class="blue">$pw</span>, PASSWORD_DEFAULT);<br>
&nbsp;<span class="red">return</span> <span class="blue">$password_hashed</span>;<br>
}<br>
        </code></div>
        <li>Fix the password variable to be encrypted.</li>
        <div class="code-block"><code>
&nbsp;<span class="blue">$passwordManager</span> = <span class="red">new</span> PasswordManager();<br>
 &nbsp;<span class="blue">$password_hashed</span> = $passwordManager->hash_password(<span class="blue">$password</span>);
        </code></div>
        <li>Connect to the database, and query a insert method.</li>
        <div class="code-block"><code>
<span class="red">public function</span> <span class="blue">query(</span>$sql<span class="blue">)</span> {<br>
&nbsp;<span class="blue">$result</span> = false;<br>
<br>
&nbsp;<span class="red">if</span> (<span class="red">isset</span>(<span class="blue">$sql</span>) && <span class="red">!empty</span>(<span class="blue">$sql</span>)) {<br>
&nbsp;&nbsp;<span class="blue">$result</span> = $this->database->query(<span class="blue">$sql</span>);<br>
&nbsp;&nbsp;<span class="red">echo</span> 'mySQL command worked';<br>
&nbsp;}<br>
&nbsp;&nbsp;else { <br>
&nbsp;&nbsp;<span class="red">echo</span> 'mySQL command in wrong';<br>
&nbsp;}  <br>
&nbsp;<span class="red">return</span> <span class="blue">$result;</span><br>
}<br>
        </code></div>
        <div class="code-block"><code>
<span class="blue">$insertQuery</span> = <span class="comment">"INSERT INTO users VALUES (NULL,'<span class="blue">$userName</span>','<span class="blue">$firstName</span>','<span class="blue">$lastName</span>','<span class="blue">$email</span>','<span class="blue">$password_hashed</span>')"</span>;<br>
<br>
<span class="blue">$db_connect</span> = <span class="red">new</span> Connect();<br>
<span class="blue">$insertUser</span> = $db_connect->query(<span class="blue">$insertQuery</span>);<br>
    </code></div>
        <li>If the query returns true(false) take to certain page using
            <div class="code-block"><code>
            <span class="comment">$dothis = (if condition)? "true" : "false";</span><br>
<span class="blue">$insertUserCompleted</span> = (<span class="blue">$insertUser</span>)? '../registration_thanks.php?registration=true' : $_SERVER["HTTP_REFERER"]. '?registration=false';<br><br>
<span class="comment">//header("Location: ***");</span><br>
<span class="red">header</span>("Location: $insertUserCompleted");
            </code></div>
        </li>

    </ol>



    <!--END MAIN CONTENT-->
    <?php
include ("includes/footer.php");
?>
