<?php
session_start();
$title = "login";
include ("includes/db_connection.php");
include ("includes/password_manager.php");
include ("includes/session_manager.php");

if (isset($_GET['submit-login'])) {
    
    $db = new Connect();
    $passwordmanager = new PasswordManager();
    
    $userName = $_GET["userName"];
    $userName = filter_var($userName, FILTER_SANITIZE_STRING);
//    Get userName from form
        
    $password = $_GET["password"];
//    Get password from form
    
    $stored_password = $db->get_password($userName);
//    Get passwords from database that matches the username
    
    $verifiedPassword = $passwordmanager->check_password($userName,$stored_password);
//    password_verify(input,input2);
    
    if($verifiedPassword){
        $sessionManager = new Session_Manager();
        $sessionManager->verify_user($userName);
        }
    else {
        echo "Your ID or password maybe wrong";
    }
    
}

?>

    <?php 
include ("includes/header.php");
include ("includes/navigation.php");
?>

        <h2>LOG IN</h2>
        <form action="<?php $_SERVER['PHP_SELF']; ?>" method="get" class="pure-form">
            <fieldset>
                <legend>Log-in:</legend>
                <input name="userName" type="text" placeholder="User Name">
                <input name="password" type="password" placeholder="Password">

                <label for="remember">
                    <input id="remember" type="checkbox">
                </label>

                <button name="submit-login" type="submit" class="pure-button pure-button-primary">Sign in</button>
            </fieldset>
        </form>

        <ol>
            <li>
                If the submit-log in is submitted, do these methods :
            </li>
            <div class="code-block"><code>
                <span class="red">if</span> (<span class="red">isset</span>($_GET['submit-login'])) { };
            </code></div>
            <li>
                First, get the input values (name and password) :
            </li>
            <div class="code-block"><code>
<span class="blue">$userName</span> = $_GET["userName"];<br>
<span class="blue">$userName</span> = <span class="red">filter_var</span>(<span class="blue">$userName</span>, <span class="red">FILTER_SANITIZE_STRING</span>);<br>
<span class="comment">//    Get userName from form
</span>        <br>
<span class="blue">$password</span> = $_GET["password"];<br>
<span class="comment">//    Get password from form
</span>
           </code></div>
           <li>
               From db_connection.php create a method get_password, and find a password matching to the user :
           </li>
           <div class="code-block"><code>
<span class="red">public function</span> <span class="blue">get_password(</span>$userName<span class="blue">)</span>{<br>
&nbsp;<span class="blue">$stored_password</span> = "";<br>
&nbsp;<span class="blue">$getMatchingUserName</span> = "SELECT * FROM users WHERE users.user_name = '<span class="blue">$userName</span>'";<br>
&nbsp;<span class="blue">$result</span> = $this->database->query(<span class="blue">$getMatchingUserName</span>);<br>
&nbsp;<br>
&nbsp;<span class="red">if</span>(<span class="blue">$result</span>-><span class="red">num_rows</span> == <span class="red">1</span>){<br>
&nbsp;&nbsp;<span class="comment">if the result has a ONE matching outcome,</span><br>
&nbsp;&nbsp;<span class="blue">$row</span> = <span class="blue">$result</span>-><span class="red">fetch_assoc</span>();<br>
&nbsp;&nbsp;<span class="comment">Fetch assoc that result!</span><br>
&nbsp;&nbsp;<span class="blue">$stored_password</span>= $row["password"];<br>
&nbsp;&nbsp;<span class="comment">Get the column password of the result into $stored_password</span><br>
&nbsp;}<br>
&nbsp;<br>
&nbsp;return <span class="blue">$stored_password</span>;<br>
}
           </code></div>
           <li>
               In password_manager.php, create a method check_password() :
           </li>
           <div class="code-block"><code>
<span class="red">public function</span> <span class="blue">check_password(</span>$password, $password_hashed<span class="blue">)</span>{<br>
 &nbsp;<span class="blue">$is_matched</span> = <span class="red">password_verify</span>(<span class="blue">$password</span>, <span class="blue">$password_hashed</span>);<br>
 &nbsp;<span class="red">return</span> <span class="blue">$is_matched</span>;<br>
 &nbsp;<span class="comment">This is return true or false</span><br>
}
           </code></div>
           <li>
               Make a boolean variable that will store if the password is correct or not :
           </li>
           <div class="code-block"><code>
<span class="blue">$verifiedPassword</span> = $passwordmanager->check_password(<span class="blue">$userName</span>,<span class="blue">$stored_password</span>);<br>
<span class="comment">//    password_verify(input,input2);
    </span>           </code></div>
       <li>
           In session_manager.php, create a method verify_user(); :
       </li>
       <div class="code-block"><code>
<span class="red">public function</span> <span class="red">verify_user</span>(<span class="blue">$userName</span>){<br>
&nbsp;$_SESSION["verified"] = <span class="red">true</span>;<br>
&nbsp;$_SESSION["userName"] = <span class="blue">$userName</span>;<br>
<br>
&nbsp;<span class="red">header</span>("Location: ./index.php");<br>
&nbsp;<span class="comment">//Takes to index when session is created</span>
}
       </code></div>
       <li>
           Now, if the password is matched(true), run verify_user to create a session array and start a session! :
       </li>
       <div class="code-block"><code>
<span class="red">if</span>(<span class="blue">$verifiedPassword</span>){<br>
&nbsp;$sessionManager = <span class="red">new</span> Session_Manager();<br>
&nbsp;<span class="comment">//connect to Session_Manager class</span><br>
&nbsp;$sessionManager->verify_user(<span class="blue">$userName</span>);<br>
&nbsp;<span class="comment">//run the verify_user method with $username input from the form</span><br>
}
       </code></div>
        </ol>

        <?php 

include ("includes/footer.php");

?>
