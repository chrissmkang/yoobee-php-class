<?php 
session_start();
$title = "logout";
include ("includes/header.php");
include ("includes/navigation.php");
include ("includes/session_manager.php");

    if(isset($_POST["submit-logout"])){
        $session_manager = new Session_Manager();
        $session_manager->destroy_session();
    }
    
?>

    <form action="<?php $_SERVER["PHP_SELF"];?>" method="post" class="pure form">
       <h3>You're logged in as : <?= $_SESSION["userName"];?></h3>
        <fieldset>
            <button name="submit-logout" type="submit" class="pure-button pure-button-primary">Log Out</button>
        </fieldset>
    </form>

    <ol>
        <li>Include session_manager.php</li>
        <div class="code-block"><code>
<span class="red">include</span> ("includes/session_manager.php");
        </code></div>
        <li>If the submit-logout button has been clicked :</li>
        <div class="code-block"><code>
<span class="red">if</span>(<span class="red">isset</span>($_POST["submit-logout"])){ };
        </code></div>
        <li>Connect to Session_Manager class and run destroy_session();</li>
        <div class="code-block"><code>
<span class="blue">$session_manager</span> = <span class="red">new</span> Session_Manager();<br>
$session_manager->destroy_session();<br>
        <br><br>
        <span class="comment">Inside of Session_Manager Class :</span><br>
<span class="red">public function</span> destroy_session(){<br>
&nbsp;        $_SESSION=[];<br>
&nbsp;<span class="comment">//empties Session array (users, pwds etc.)</span><br>
<br>     
&nbsp;        session_destroy();<br>
&nbsp;<span class="comment">//destroy session</span><br>
<br>     
&nbsp;        header("Location: ./index.php");<br>
&nbsp;<span class="comment">//After destroying, takes user to index</span><br>
}
        

        </code></div>
    </ol>

    <?php 
include ("includes/footer.php");
?>
