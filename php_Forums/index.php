<?php
session_start();
include ("includes/header.php");
include ("includes/navigation.php");
include ("includes/db_connection.php");
include ("functions/utils.php");

$db = new Connect();
$allforums_array= $db->get_all_forums();

//dumpObject($allforums_array);
?>
    <!--MAIN CONTENT-->


    <div class="home">
        <h1>This is a home page!</h1>
        <h3>Welcome to our YOOBEE FORUM</h3>
    </div>

    <table class="forum-table">
        <thead>
            <tr>
                <th>Forum ID</th>
                <th>Forum Name</th>
                <th>Updated</th>
            </tr>
        </thead>
        <tbody>
            <?php for($i =0; $i <count($allforums_array); $i++) : ?>
                <tr>
                    <td>
                        <?= $allforums_array[$i][1];?>
                    </td>
                    <td>
                        <a href="./view_thread.php?id=<?=$allforums_array[$i][1]?>">
                            <?= $allforums_array[$i][0];?>
                        </a>
                    </td>
                    <td>
                        <?php 
                        $updatedDate = $db->get_updated_date($i+1);
                        print_r($updatedDate);
                        ?>
                    </td>
                </tr>
                <?php endfor;?>
        </tbody>
    </table>

    <ol>
        <li>In db_connection.php create get_all_forums() :</li>
        <div class="code-block"><code>
<span class="red">public function</span> <span class="blue">get_all_forums()</span>{<br>
&nbsp;<span class="blue">$sql</span> = "SELECT * FROM forums order by forum_id";<br>
&nbsp;<span class="comment">//A string of 'Get all forum tables</span><br>
&nbsp;<span class="blue">$result</span> = $this->database->query($sql);<br>
   &nbsp;<span class="comment">//$result stores the data from db</span><br>
    <br>
&nbsp;<span class="blue">$forums_array</span> = [];<br>
&nbsp;<span class="comment">$forums_array is currently empty</span><br>
<br>
&nbsp;if(<span class="blue">$result</span>->num_rows > 0) {<br>
&nbsp;<span class="comment">if there are ANY results found</span><br>
&nbsp;&nbsp;while (<span class="blue">$row</span> = <span class="blue">$result</span>->fetch_assoc()) {<br>
&nbsp;&nbsp;<span class="comment">//Fetch_assoc each one of them into $row</span><br>
&nbsp;&nbsp;&nbsp;array_push (<span class="blue">$forums_array</span>, [<span class="blue">$row</span>["name"],<span class="blue">$row</span>["forum_id"]] );<br>
&nbsp;&nbsp;&nbsp;<span class="comment">//Push the $row with name column, and $row with forum_id into $forum_array</span><br>
&nbsp;&nbsp;}<br>
&nbsp;}<br>
&nbsp;return <span class="blue">$forums_array</span>;<br>
&nbsp;<span class="comment">//the method returns $forum_array with two arrays (0 and 1) storing name and forum_id value from $row</span><br>
}
        </code></div>
        <li>In db_connection.php create get_updated_date():</li>
        <div class="code-block"><code>
        
<span class="red">public function</span> <span class="blue">get_updated_date</span>($id){<br>
&nbsp;<span class="blue">$sql</span> = "SELECT MAX(date_entered) FROM messages WHERE messages.forum_id = $id ORDER BY date_entered DESC";<br>
&nbsp;<span class="blue">$result</span> = $this->database->query(<span class="blue">$sql</span>);<br>
&nbsp;<span class="blue">$updatedDate</span> = "";<br>
<br>
&nbsp;<span class="red">if</span>(<span class="blue">$result</span>->num_rows == 1) {<br>


&nbsp;&nbsp;<span class="blue">$row</span> = <span class="blue">$result</span>-><span class="red">fetch_assoc</span>();<br>
<br>
&nbsp;&nbsp;<span class="red">if</span>(<span class="blue">$row</span>["MAX(date_entered)"]){<br>
&nbsp;&nbsp;&nbsp;<span class="blue">$updatedDate</span> = <span class="blue">$row</span>["MAX(date_entered)"];<br>
&nbsp;&nbsp;} <span class="red">else</span> { <br>
&nbsp;&nbsp;&nbsp;<span class="blue">$updatedDate</span> = "Empty Forum";<br>
&nbsp;&nbsp;}<br>
&nbsp;}<br>


&nbsp;<span class="red">return</span> <span class="blue">$updatedDate</span>;<br>
}
        </code></div>

        <li>The HTML for this table is :</li>
        <div class="code-block"><code>
<span class="blue">$db</span> = <span class="red">new</span> Connect();<br>
<span class="blue">$allforums_array</span>= $db-><span class="blue">get_all_forums</span>();<br>
<br>
&lt;<span class="yellow">table</span> <span class="red">class</span>="forum-table"><br>
&nbsp;&lt;<span class="yellow">thead</span>><br>
&nbsp;&nbsp;&lt;<span class="yellow">tr</span>><br>
&nbsp;&nbsp;&nbsp;    &lt;<span class="yellow">th</span>>Forum ID&lt;/<span class="yellow">th</span>><br>
&nbsp;&nbsp;&nbsp;    &lt;<span class="yellow">th</span>>Forum Name&lt;/<span class="yellow">th</span>><br>
&nbsp;&nbsp;&lt;/<span class="yellow">tr</span>><br>
&nbsp;&lt;/<span class="yellow">thead</span>><br>
&nbsp;&lt;<span class="yellow">tbody</span>><br>
&nbsp;&nbsp;&lt;?php <span class="red">for</span>(<span class="blue">$i</span> = <span class="red">0</span>; <span class="blue">$i</span> &lt; <span class="red">count</span>(<span class="blue">$allforums_array</span>); <span class="blue">$i</span>++) : ?><br>
&nbsp;&nbsp;&lt;<span class="yellow">tr</span>><br>
&nbsp;&nbsp;&nbsp;&lt;<span class="yellow">td</span>>&lt;?= <span class="blue">$allforums_array</span>[<span class="blue">$i</span>][1];?>&lt;/<span class="yellow">td</span>><br>
&nbsp;&nbsp;&nbsp;&lt;<span class="yellow">td</span>><br>
&nbsp;&nbsp;&lt;<span class="yellow">a</span> <span class="red">href</span>="<span class="comment">./view_thread.php?id=</span>&lt;?=<span class="blue">$allforums_array</span>[<span class="blue">$i</span>][<span class="red">1</span>]?>">&lt;?= <span class="blue">$allforums_array</span>[<span class="blue">$i</span>][<span class="red">0</span>];?>&lt;/<span class="yellow">a</span>><br>
&nbsp;&nbsp;&nbsp;&lt;<span class="yellow">td</span>><br>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;?php <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="blue">$updatedDate</span> = $db-><span class="blue">get_updated_date</span>(<span class="blue">$i</span>+<span class="red">1</span>);<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="red">print_r</span>(<span class="blue">$updatedDate</span>);<br>
&nbsp;&nbsp;&nbsp;&nbsp;?><br>
&nbsp;&nbsp;&nbsp;&lt;/<span class="yellow">td</span>><br>
&nbsp;&nbsp;&nbsp;&lt;/<span class="yellow">td</span>><br>
&nbsp;&nbsp;&lt;/<span class="yellow">tr</span>><br>
&nbsp;&nbsp;&lt;?php <span class="red">endfor</span>;?><br>
&nbsp;&lt;/<span class="yellow">tbody</span>><br>
&lt;/<span class="yellow">table</span>><br>
        </code></div>
    </ol>

    <!--END MAIN CONTENT-->
    <?php
include ("includes/footer.php");
?>
