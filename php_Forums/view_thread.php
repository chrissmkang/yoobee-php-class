<?php
session_start();
$title = "Yoobee Forum Thread id ".$_GET["id"];
include ("includes/header.php");
include ("includes/navigation.php");
include ("includes/db_connection.php");
include ("includes/session_manager.php");


$db = new Connect();

$forum_name = $db->get_forum_name($_GET["id"]);
$posts = $db->get_forum_posts($_GET["id"]);

?>

    <h2>Forum : "<?= $forum_name; ?>"</h2>

    <?php for($i = 0; $i < count($posts); $i++) : ?>

        <h3><?= $posts[$i]["subject"];?></h3>
        <p><?= $posts[$i]["body"];?></p>

        <?php endfor; ?>
        
        
        <ol>
            <li>Connect to database Connect class</li>
            <div class="code-block"><code>
<span class="blue">$db</span> = <span class="red">new</span> Connect();
            </code></div>
            <li>Run get_forum_posts with the id from GET url</li>
            <div class="code-block"><code>
<span class="blue">$posts</span> = $db-><span class="blue">get_forum_posts</span>(<span class="blue">$_GET</span>["<span class="red">id</span>"]);
            </code></div>
            <li>get_forum_posts method :</li>
            <div class="code-block"><code>
<span class="red">public function</span> <span class="blue">get_forum_posts</span>(<span class="blue">$id</span>) {<br>
&nbsp;<span class="blue">$sql</span> = <span class="comment">"SELECT * FROM messages WHERE messages.forum_id = </span><span class="blue">$id</span>";<br>
&nbsp;<span class="blue">$result</span> = $this->database->query(<span class="blue">$sql</span>);<br>
&nbsp;<br>
&nbsp;<span class="blue">$messages</span> = [];<br>
<br>
&nbsp;<span class="red">if</span>(<span class="blue">$result</span>-><span class="red">num_rows</span> > <span class="red">0</span>) {<br>
&nbsp;&nbsp;<span class="red">while</span> (<span class="blue">$row</span> = $result-><span class="red">fetch_assoc</span>()) {<br>
&nbsp;&nbsp;&nbsp;<span class="red">array_push</span>(<span class="blue">$messages</span>,<span class="blue">$row</span>);<br>
&nbsp;&nbsp;}<br>
&nbsp;}<br>
&nbsp;<span class="red">return</span> <span class="blue">$messages</span>;<br>
<span class="comment">//get_forum_posts() returns an array containing body and subject</span><br>
}
            </code></div>
            <li>The HTML for this page :</li>
            <div class="code-block"><code>
&lt;?php <span class="red">for</span>(<span class="blue">$i</span> = <span class="red">0</span>; <span class="blue">$i</span> &lt; <span class="red">count</span>(<span class="blue">$posts</span>); <span class="blue">$i</span>++) : ?><br>
<br>
&lt;<span class="yellow">h3</span>>&lt;?= <span class="blue">$posts</span>[<span class="blue">$i</span>]['<span class="red">subject</span>'];?> &lt;/<span class="yellow">h3</span>><br>
&lt;<span class="yellow">p</span>>&nbsp;&lt;?= <span class="blue">$posts</span>[<span class="blue">$i</span>]['<span class="red">body</span>'];?> &lt;/<span class="yellow">p</span>><br>
<br>
&lt;?php <span class="red">endfor</span>; ?>
            </code></div>
        </ol>


            <?php
include ("includes/footer.php");
?>
