<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Yoobee Forum</title>
    <link rel="stylesheet" href="css/styles.css" type="text/css">
    <link rel="stylesheet" href="css/forms-min.css" type="text/css">
</head>

<body>
    <h2>Yoobee Forum</h2>
    <h4>Table Names</h4>
    <ul>
        <li>users</li>
        <li class="code-block"><code>
<span class="red">CREATE TABLE IF NOT EXISTS</span> <span class="blue">users</span> ( <br>
&nbsp;<span class="blue">user_id</span> <span class="yellow">mediumint unsigned NOT NULL AUTO_INCREMENT</span>,<br>
&nbsp;<span class="blue">user_name</span> <span class="yellow">varchar(255) NOT NULL</span>,<br>
&nbsp;<span class="blue">first_name</span> <span class="yellow">varchar(255) NOT NULL</span>, <br>
&nbsp;<span class="blue">last_name</span> <span class="yellow">varchar(255) NOT NULL</span>, <br>
&nbsp;<span class="blue">email</span> <span class="yellow">varchar(255) NOT NULL</span>, <br>
&nbsp;<span class="blue">password</span> <span class="yellow">varchar(255) NOT NULL</span>, <br>
&nbsp;<span class="red">PRIMARY KEY</span> (<span class="blue">user_id</span>) <br>
)
        </code></li>
        <li class="code-block"><code>
            <span class="red">ALTER TABLE</span> `<span class="blue">users</span>` <span class="yellow">ADD UNIQUE</span>(`<span class="blue">user_name</span>`);
        </code></li>
        <li>forums</li>
        <li class="code-block"><code>
<span class="red">CREATE TABLE IF NOT EXISTS</span> <span class="blue">forums</span> ( <br>
&nbsp;<span class="blue">forum_id</span> <span class="yellow">mediumint unsigned NOT NULL AUTO_INCREMENT</span>,<br>
&nbsp;<span class="blue">name</span> <span class="yellow">varchar(255) NOT NULL unique</span>,<br>
&nbsp;<span class="red">PRIMARY KEY</span> (<span class="blue">forum_id</span>) <br>
)
        </code></li>
        <li>messages</li>
        <li class="code-block"><code>
<span class="red">CREATE TABLE IF NOT EXISTS</span> <span class="blue">messages</span> ( <br>
&nbsp;<span class="blue">message_id</span> <span class="yellow">mediumint unsigned NOT NULL AUTO_INCREMENT</span>,<br>
&nbsp;<span class="blue">forum_id</span> <span class="yellow">int unsigned NOT NULL</span>,<br>
&nbsp;<span class="blue">parent_id</span> <span class="yellow">int unsigned NOT NULL</span>,<br>
&nbsp;<span class="blue">user_id</span> <span class="yellow">int unsigned NOT NULL</span>,<br>
&nbsp;<span class="blue">subject</span> <span class="yellow">varchar(500) NOT NULL</span>,<br>
&nbsp;<span class="blue">body</span> <span class="yellow">longtext NOT NULL</span>,<br>
&nbsp;<span class="blue">date_entered</span> <span class="yellow">datetime NOT NULL</span>,<br>
&nbsp;<span class="red">PRIMARY KEY</span> (<span class="blue">message_id</span>) <br>
)
        </code></li>
    </ul>
    
    <a href="index.php">Go to index.php</a>
</body>

</html>
