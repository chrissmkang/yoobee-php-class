<?php 

class PasswordManager {
    
    public function __construct() {
        
    }
    
    public function hash_password($pw) {
        $password_hashed = password_hash($pw, PASSWORD_DEFAULT);
        return $password_hashed;
    }
    
    public function check_password($password, $password_hashed){
        $is_matched = password_verify($password, $password_hashed);
        return $is_matched;
    }
    
    
}


?>