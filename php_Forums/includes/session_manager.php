<?php 

include_once("functions/utils.php");

class Session_Manager {
    
    public function __construct(){}
    
    public function verify_user($userName){
        $_SESSION["verified"] = true;
        $_SESSION["userName"] = $userName;
        
        header("Location: ./index.php");

    }
    
    public function destroy_session(){
        
        $_SESSION=[];
//        empties Session array (users, pwds etc.)
        
        session_destroy();
//        destroy session
        
        header("Location: ./index.php");
//        After destroying, takes user to index
    }
    
    
}


?>