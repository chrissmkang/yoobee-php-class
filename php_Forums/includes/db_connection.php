<?php 

define("DB_SERVER", "127.0.0.1");
define("DB_USER", "root");
define("DB_PASSWORD", "");
define("DB_NAME", "yoobee_forum");

class Connect {

    private $database;
    
    public function __construct(){
        $this->database = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME); 
    
    if ($this->database->connect_error) {
        echo '<!-- oh dear, db connection is not good -->';
        die ("db error, i'm dead");
        } else { 
        echo '<!-- the db connection is good -->'; 
        } 
    }
    
    public function __destruct() {
        $this->database->close(); 
    }
    
    public function query($sql) {
        $result = false;
        
        if (isset($sql) && !empty($sql)) {
            $result = $this->database->query($sql);
            echo 'mySQL command worked<br>';
        }
        else { 
            echo 'mySQL command in wrong<br>';
        }  
        return $result;
    }
    
    public function get_password($userName){
        $stored_password = "";
        $getMatchingUserName = "SELECT * FROM users WHERE users.user_name = '$userName'";
        $result = $this->database->query($getMatchingUserName);
        
        if($result->num_rows == 1){
            $row = $result->fetch_assoc();
            $stored_password= $row["password"];
        }
        
        return $stored_password;
    }
    
    public function get_all_forums(){
        $sql = "SELECT * FROM forums order by forum_id";
        $result = $this->database->query($sql);
        
        $forums_array = [];
        
        if($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                array_push ($forums_array, [$row["name"], $row["forum_id"]] );
            }
        }
        
        return $forums_array;
    }
    
    public function get_forum_posts($id) {
        $sql = "SELECT * FROM messages WHERE messages.forum_id = $id";
        $result = $this->database->query($sql);
        
        $messages = [];
        
        if($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                array_push($messages,$row);
            }
        }
        return $messages;
//get_forum_posts() returns an array containing body and subject
    }
    
    public function get_updated_date($id){
        $sql = "SELECT MAX(date_entered) FROM messages WHERE messages.forum_id = $id ORDER BY date_entered DESC";
        $result = $this->database->query($sql);
        $updatedDate = "";
        
        if($result->num_rows == 1) {
            $row = $result->fetch_assoc();
            if($row["MAX(date_entered)"]){
                $updatedDate = $row["MAX(date_entered)"];
            } else {
                $updatedDate = "Empty Forum";
            }
        }
        return $updatedDate;
    }
    
    public function get_forum_name($id) {
        $sql = "SELECT name FROM forums WHERE forums.forum_id = $id";
        $result = $this->database->query($sql);
        
        $forum_name = "";
        
        if($result->num_rows == 1) {
            $row = $result->fetch_assoc();
            $forum_name = $row["name"];
        }
        return $forum_name;
    }
    
    public function truncate() {
        $truncateTable = 'truncate table users';
        echo $truncateTable;
        $this->query($truncateTable);
    }

} 

?>
