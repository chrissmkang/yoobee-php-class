<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title><?= isset($title) ? $title : 'YOOBEE FORUM';?></title>
    <link rel="stylesheet" href="css/forms-min.css" type="text/css">
    <link rel="stylesheet" href="css/styles.css" type="text/css">

</head>

<body>
