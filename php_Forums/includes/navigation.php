<?php if (isset($_SESSION["verified"]) && $_SESSION["verified"]): ?>
    <!--When logged in-->
    <nav>
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="logout.php">Logout</a></li>
            <li><a href="new_forum.php">New Forum</a></li>
            <li><a href="new_message.php">New Message</a></li>
        </ul>
    </nav>

    <?php else: ?>
        <!--When logged out-->
        <nav>
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="registration.php">Registration</a></li>
                <li><a href="login.php">Login</a></li>
                <li><a href="new_forum.php">New Forum</a></li>
                <li><a href="new_message.php">New Message</a></li>
            </ul>
        </nav>

        <?php endif; ?>
