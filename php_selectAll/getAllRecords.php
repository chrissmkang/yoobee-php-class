<?php 

//echo phpinfo(); - good for testing

class Connect {
    private $database_connect;
    
    
    public function __construct(){
        $this->database_connect = new mysqli("127.0.0.1","root","","select_queries");
        //host, user, password, databaseName
    
        if($this->database_connect->connect_error){
            die("could not connect to db");
        } else {
            echo "Database Connected <br>";
        }
    }
    
    public function send_query($sql) {
        echo "Query has been received! (send_query function)<br><br>";
        
        $query = $this->database_connect->query($sql);
        //$query stores the SQL query
                
        if ($query && $query->num_rows ) {
            //if a query has been received AND has an info
            
            echo "The num_rows of this records are :". $query->num_rows;
            //num_rows shows the number of records in a table
            
            echo "<br><br>";
            
            $counter = 0;
            
            while( $records = $query->fetch_assoc()) {
                //$Loop all queries
                //Allows to get columns by $records["columnName"]
                
                $counter++; //counter goes up from 0 as rows loop
                
                echo '<li>';
                echo "ID : ". $records['id'] ." || ";
                echo $records['description'] . " = " ;
                echo ($records["completed"] === "1")? "done" : "not done";
        
//                echo "<pre>";
//                print_r($records);
//                echo "</pre>";
                echo "</li>";
            }
            echo "<hr>";
        }
        
    }
    
}


if (isset($_GET['submit_button'])){
    echo "<h2>SUBMIT CLICKED -> FORM RECEIVED</h2><br>";
    
    echo "database connecting.....<br>";//Instansiate $db as Class Connect();
    $db = new Connect();

    
    echo "Send query! <hr>";//Send through query to database;

    $db->send_query("SELECT * FROM select_queries_tbl where (id>0) ORDER BY completed ASC");
    //MySQL command = "Select everything from "select_queries_tbl" table, where id is greater than 0. Order them by completion ascending(0 first).
}

?>
