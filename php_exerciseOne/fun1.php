<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>php introduction</title>
</head>

<body>
    <h1>Form from exercise One</h1>


    <?php

	//Checking our input
    echo "<pre>";
	print_r($_POST);
    echo "</pre>";
    
    if ( isset($_POST["submit"]) && !empty($_POST["age"]) ){
    $iAge = $_POST["age"];
    //OR
    //$iAge = htmlentities($_POST["age"]);


	//Processing

	$sActivity = "";

	if($iAge<10){
		$sActivity = "Have an icecream";
	} else if ($iAge>64){
		$sActivity = "Have a rest";
	} else {
		$sActivity = "Enjoy a beer or two";
	}

	//Output
	echo "<h2>Hi there! You are ".$iAge."</h2>";
	echo "<h2>".$sActivity."</h2>";
    } else {
        echo '<h1> NO GOOD! WRITE DOWN YOUR AGE!!!!!</h1>';
    }


	?>


</body>

</html>