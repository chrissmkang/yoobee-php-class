<?php 

class Upload{
    
    public $img_name;
    public $img_tmp_name;
    
    public function __construct($img_name, $img_tmp_name){
        $this->img_name = $img_name;
        $this->img_tmp_name = $img_tmp_name;
    }
    
    public function existCheck(){
        $fileNameArray = explode(".",$this->img_name);
        $fileExtension = end($fileNameArray);
        $tempFile = $this->img_tmp_name;
        
        if(file_exists("../images/".$this->img_name)){
            exit;
        }
    }
    
    public function main(){
        $this->existCheck();
        
        move_uploaded_file($this->img_tmp_name, "../images/".$this->img_name);
    }
    
    
}



?>
