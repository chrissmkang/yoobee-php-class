<?php 
include("includes/header.php");
include("managers/db_manager.php"); 

$db = new db_manager;
$msgGrps = $db->getAllMessageGrps();

?>
    <div id="new_message_page">
        <h3>New Message</h3>

        <form class="newMsgFrm" action="./methods/update_messages.php" method="post" enctype="multipart/form-data" name="newMessageForm">
            <label for="title">Title</label>
            <input type="text" name="title" required>

            <label for="image_upload"></label>
            <input type="file" name="image_upload" required>

            <label for="msgGrpSelect">Message Group</label>
            <select name="group_id">
                <?php foreach($msgGrps as $msgGrp) : ?>
                    <option value="<?= $msgGrp['code']; ?>">
                        <?= $msgGrp['title'] ?>
                    </option>
                    <?php endforeach; ?>
            </select>


            <label for="message">Message</label>
            <textarea name="message" id="" cols="30" rows="10"></textarea>

            <input type="submit" name="new_msg_submit" value="Post">
        </form>
    </div>
  
            <?php include("includes/footer.php") ?>
