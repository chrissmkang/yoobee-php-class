<?php 
include("includes/header.php");
include("managers/db_manager.php"); 

$db = new db_manager;

$allMessagesGroups = $db->getAllMessageGrps();

if(isset($_GET["grpID"])){
    $selectedMessage = $db->getMessageData($_GET["grpID"]);    
} else {
    $selectedMessage = $db->getAllMessageData();
}


?>
    <!--   BODY-->

    <div id="home">
        <ul class="message_grps">
            <li><a href="<?= $_SERVER['PHP_SELF']; ?>">All</a></li>
            <?php foreach($allMessagesGroups as $msgGrp): ?>
                <li>
                    <a href='
                    <?= $_SERVER["PHP_SELF"]."?grpID=".$msgGrp["group_id"]; ?>
                    '>
                        <?= $msgGrp["title"]?>
                    </a>
                </li>
                <?php endforeach; ?>
        </ul>

        <ul class="message_th">

            <?php foreach($selectedMessage as $msg): ?>
                <li>
                    <div class="message_content">
                        <a href=''>
                            <img src="./images/<?= $msg["image"]; ?>" alt="">                        
                            <h5>
                                <?= $msg["content_title"];?>
                                <p>
                                <?php 
                                $userName = $db->getUserNameForMessage($msg["user_id"]);
                                echo "by ";
                                print_r($userName[0]["first_name"]);
                                echo " ";
                                print_r($userName[0]["last_name"]);
                                
                                ?>
                            </p>
                            </h5>
                            
                        </a>
                    </div>
                </li>
                <?php endforeach; ?>



        </ul>
    </div>


    <?php include("includes/footer.php") ?>
