<?php 

DEFINE("HOST","127.0.0.1");
DEFINE("USER","root");
DEFINE("PASSWORD","");
DEFINE("DATABASE","chris_blog");

class db_manager {
    
    private $db;
    
    public function __construct(){
        $this->db = new mysqli(HOST,USER,PASSWORD,DATABASE);
        if($this->db->connect_error) {
            //echo "database not connected";
            die();
        } else {
            //echo "database connected";
        }
    }
    
    public function getAllMessageGrps(){
        $sql = "SELECT * FROM message_groups";
        $result = $this->db->query($sql);
        $allMessages = [];
        
        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
                array_push($allMessages,$row);
            }
        }
        return $allMessages;
    }
    
    public function getMessageData($grpID){
        $sql = "SELECT * FROM messages WHERE group_ID = $grpID";
        $result = $this->db->query($sql);
        $messageData = [];
        
        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
                array_push($messageData,$row);
            }
        }
        return $messageData;
    }
    
    public function getAllMessageData(){
        $sql = "SELECT * FROM messages";
        $result = $this->db->query($sql);
        $messageData = [];
        
        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
                array_push($messageData,$row);
            }
        }
        return $messageData;
    }
    
    public function getUserNameForMessage($uID_f_Msg){
        $sql = "SELECT first_name, last_name FROM users WHERE user_id = $uID_f_Msg";
        $result = $this->db->query($sql);
        $messageData = [];
        
        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
                array_push($messageData,$row);
            }
        }
        return $messageData;
    }
    
    
    public function query($sql) {
        $result = false;
        if (isset($sql) && !empty($sql)) {
            echo 'mySQL command worked<br>';
            $result = $this->db->query($sql);
        }
        else { 
            echo 'mySQL command in wrong<br>';
        }
        return $result;   //$result is a boolean 
    } 
    
}


?>
