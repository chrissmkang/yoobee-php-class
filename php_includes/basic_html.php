<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>html include</title>
    <style>
        footer {
            color: plum;
            border: 2px dashed black;
        }
        code {
            background-color: #333;
            color: #fff;
            padding: 0.3em;
            border-radius: 5px;
        }

    </style>
</head>

<body>

    <div>
        <h3>This page has included a footer.html file using 
        <code>include "include/footer.html"</code>
        </h3>
    </div>


    <?php
include "includes/footer.html";
?>

        <div style="width: 70vw; margin: auto; margin-top:10em">
            <p>Down below, I have included the home page using <code>include "../index.html"</code>
</p>
            <p>However, CSS styles are not transferred, and will cause error on console.</p>
            <?php 
    include "../index.html";
    ?>
        </div>


</body>

</html>
