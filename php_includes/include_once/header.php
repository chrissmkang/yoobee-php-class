<?php 
include_once("includes/functions.php"); 
include("includes/globals.php"); 
foo(); 
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>includes_once</title>
    <style>
        code {
            background-color: #333;
            color: #fff;
            padding: 0.3em;
        }
    </style>
</head>
<body>
   <p style="margin-top:10em;">This page has loaded  <u>functions.php</u> and <u>globals.php</u></p>
   
   <p>In functions.php, it has a function called foo();</p>
   <code>function foo() { echo " Some code from, somewhere! ";}</code>
   <p>In global.php, it <code>include_once("function.php");</code> and calls in the function <code>foo();</code></p>
   <p><code>include_once</code> calls in function once so it doesn't clash with other php files.</p>
   <p>In this case, function.php is ignored, as its function overlaps with globals.php</p>
    
</body>
</html>