<html>

<head>
    <title>PHP Test</title>
    <style>
        a,
        button,
        p,
        ,
        h3,
        li {
            font-family: helvetica;
        }
        
        a {
            position: relative;
            display: inline-block;
            text-decoration: none;
            margin: 1em;
        }
        
        button {
            color: white;
            padding: 1em;
            border-radius: 5%;
            background-color: dodgerblue;
            outline: none;
        }
        code{
            background-color: #333;
            color: #fff;
            padding: 0.1em;
            border-radius: 5%;
        }

    </style>
</head>

<body>
    <a href="choosePicture.php">
        <button>choosePicture.php</button>
    </a>
    <a href="choosePictureSimple.php">
        <button>choosePictureSimple.php</button>
    </a>
    <a href="phpinfo.php">
        <button>php Info</button>
    </a>

    <h3>On this form:</h3>

    <p>For kitten, <code>echo "&lt;img src={$imageInfoArray["path"]}>";</code> is used to load the image rather than typing in the actual image path</p>
    <p>Check out codes on kitten for better understanding.</p>

    <h3>New things learnt</h3>
    <ul>
        <li>You can use function $function(){return[];} to produce an empty array. Then it could be assigned to a new array variable</li>
        <li>$_SERVER["PHP_SELF"] refers to the php page itself, rather than linking the form to another php file.</li>
    </ul>


    <form Name="f1" action="<?php $_SERVER["PHP_SELF"] ?>" method="post">
        <!--php $server php_self can be replaced to the name of this file (e.g choosePicture.php), however, writing in this method prevents if the name of the file has been changed-->
        <select name="picture">
            <option value="none">Select a Picture</option>
            <option value="church">A Church</option>
            <option value="kitten">Kitten</option>
            <option value="planet">Planet</option>
            <option value="cartoon">Cartoon</option>
            <option value="space">Space Image</option>
            <option value="abstract">Photoshop Abstract</option>
        </select>

        <input type="submit" name="Submit" Value="Choose an Image">
    </form>

    <?php
        
        define ("IMAGES_PATH", "images/");
//          check church path
        
        if (isset($_POST['Submit']))
        {
            $picture = $_POST['picture'];
                        
            switch ($picture)
            {
                case 'kitten':

                    $imageInfoArray = getImageSizeAsArray("images/kitten.jpg");
//                      echo $imageInfoArray["path"] ; 
                    echo "<img src={$imageInfoArray["path"]}>";
                    
                    echo "<pre>";
                  print_r($imageInfoArray);
                  echo "</pre>";
                    
                    break;

                case 'church':
                    $imageInfoArray = getImageSizeAsArray(IMAGES_PATH . "church.jpg");
                   echo "<img src={$imageInfoArray["path"]}>";
                    break;

                case 'planet':
                    $imageInfoArray = getImageSizeAsArray("images/planet.jpg");
                    echo "<img src=images/planet.jpg>";
                    break;

                case 'cartoon':
                    $imageInfoArray = getImageSizeAsArray("images/cartoon.jpg");
                    echo "<img src=images/cartoon.jpg>";
                    break;

                case 'space':
                    $imageInfoArray = getImageSizeAsArray("images/stellar.jpg");
                    echo "<img src=images/stellar.jpg>";
                    break;

                case 'abstract':
                    $imageInfoArray = getImageSizeAsArray("images/abstract.jpg");
                    echo "<img src=images/abstract.jpg>";
                    break;

                default:
                    echo ("No Image Selected");
            }
        }

              function getImageSizeAsArray($path)
              {
                  $tempArray = getimagesize($path);

                  echo "<pre>";
                  print_r($tempArray);
                  echo "</pre>";

                  return ["path"=> $path, "width" => $tempArray[0], "height" => $tempArray[1]];
//This function calls in the image path, then with getimagesize(), it gets the neccessary informations, which are returned into an empty array below.
                  
//This function now returns an empty array with keys, 'path', 'width', and 'height'. 
              }

        ?>
</body>

</html>
