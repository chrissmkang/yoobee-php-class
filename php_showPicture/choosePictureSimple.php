<html>

<head>
    <title>PHP Test</title>
    <style>
        a {
            position: relative;
            display: inline-block;
            text-decoration: none;
            margin: 1em;
        }
        
        button {
            color: white;
            padding: 1em;
            border-radius: 5%;
            background-color: dodgerblue;
            outline: none;
        }

    </style>
</head>

<body>
    <a href="choosePicture.php">
        <button>choosePicture.php</button>
    </a>
    <a href="choosePictureSimple.php">
        <button>choosePictureSimple.php</button>
    </a>
    <a href="phpinfo.php">
        <button>php Info</button>
    </a>

    <form Name="f1" action="showPicture.php" method="post">
        <select name="picture">
            <option value="none">Select a Picture</option>
            <option value="church">A Church</option>
            <option value="kitten">Kitten</option>
            <option value="planet">Planet</option>
            <option value="cartoon">Cartoon</option>
            <option value="space">Space Image</option>
            <option value="abstract">Photoshop Abstract</option>
        </select>

        <input type="submit" name="Submit" Value="Choose an Image">
    </form>

</body>

</html>
