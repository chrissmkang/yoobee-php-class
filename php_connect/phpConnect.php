<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>php Using Connect</title>
    <style>
        * {
            font-family: helvetica;
        }
        
        h1 {
            text-align: center;
        }
        
        .code-block {
            background-color: #333;
            color: #fff;
            padding: 1em;
            width: auto;
        }
        
        code,
        code >* {
            font-family: monospace;
        }
        .imp {
            color: tomato;
        }
        i {
            color:dodgerblue;
        }
        .yellow {
            color:yellow;
        }
        .comment {
            color: grey;
        }
    </style>
</head>

<body>
    <h1>Class to connect php and MySQL</h1>

   <div class="code-block">
       <code>
           &lt;?php<br><br>
           
           <span class="imp">define</span>("<span class="yellow">DB_SERVER</span>", "127.0.0.1");<br>
           <span class="imp">define</span>("<span class="yellow">DB_USER</span>", "root");<br>
           <span class="imp">define</span>("<span class="yellow">DB_PASSWORD</span>", "");<br>
           <span class="imp">define</span>("<span class="yellow">DB_NAME</span>", "<i>dataBaseName</i>");<br><br>
           
           <span class="imp">class</span> <i>Connect</i> {<br><br>
           &nbsp;<span class="imp">private</span> <i>$dataBase</i>;<br><br>
           &nbsp;<span class="imp">public function</span> <i>__construct()</i>{<br>
           &nbsp;&nbsp; $this-><i>database</i> = <span class="imp">new</span> <i>mysqli</i>(<span class="yellow">DBSERVER</span>, <span class="yellow">DB_USER</span>, <span class="yellow">DB_PASSWORD</span>, <span class="yellow">DB_NAME</span>);<br>
           <br>
           &nbsp;&nbsp;<span class="imp">if </span>($this-><i>database</i>-><span class="yellow">connect_error</span>) {<br>
           &nbsp;&nbsp;&nbsp;echo <span class="yellow">'&lt;!-- oh dear, db connection is not good -->'</span>;<br>
           &nbsp;&nbsp;&nbsp;die (<span class="yellow">"db error, i'm dead"</span>);<br>
           &nbsp;&nbsp; } <span class="imp">else</span> { <br>
           &nbsp;&nbsp;&nbsp;echo '<span class="yellow">&lt;!-- the db connection is good --></span>';
           <br>&nbsp;&nbsp;&nbsp;}
           <br>&nbsp;&nbsp;}<br><br>
           &nbsp;<span class="imp">public function</span> <i>__destruct()</i> {<br>
           &nbsp;&nbsp;echo <span class="yellow">'database closed&lt;br>'</span>;<br>
           &nbsp;&nbsp;$this-><i>dataBase</i>-><span class="yellow">close()</span>;
           <br>&nbsp;}<br><br>
           &nbsp;<span class="imp">public function</span> <i>query</i>($sql) {<br>
           &nbsp;&nbsp;$result = false;<br>
           &nbsp;&nbsp;<span class="imp">if</span> (<span class="imp">isset</span>($sql) <span class="imp">&&</span> <span class="imp">!empty</span>($sql)) {<br>
           &nbsp;&nbsp;&nbsp;echo <span class="yellow">'mySQL command worked&lt;br>'</span>;<br>
           &nbsp;&nbsp;&nbsp;$result = $this-><i>database</i>-><span class="yellow">query</span>($sql);<br>
          &nbsp; &nbsp;}<br>
           &nbsp;&nbsp;<span class="imp">else</span> { <br>
           &nbsp;&nbsp;&nbsp;echo '<span class="yellow">mySQL command in wrong&lt;br></span>';<br>
           &nbsp;&nbsp;}<br>
           &nbsp;&nbsp;<span class="imp">return</span> $result;
           &nbsp;&nbsp;<span class="comment">//$result is a boolean</span>
           <br>&nbsp;}
           <br><br>}
       </code>
   </div>
    
</body>

</html>
