<?php

define("DB_SERVER", "127.0.0.1");
define("DB_USER", "root");
define("DB_PASSWORD", "");
define("DB_NAME", "toDos");

class Connect {
    
    private $db_exercise;
    
    
    public function __construct(){
        echo 'database connected<br>';
        $this->db_exercise = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME);
        
        if ($this->db_exercise->connect_error) {
            echo '<!-- oh dear, db connection is not good -->';
            die("db error, I'm out of here");
        } else {
            echo '<!-- happy days, the db connection is good -->';
            }
    }
    
    public function __destruct(){
        echo 'database closed<br>';
        $this->db_exercise->close();

    }
    
    public function query($sql) {
        
        $result = $this->db_exercise->query($sql);
        
        if ($result == true) {
            echo 'mySQL command worked<br>';
        }
        else {
            echo 'mySQL command is wrong<br>';
        }
    }
    
    
}


?>
