<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Thriftee</title>
    <link rel="stylesheet" href="style.css" type="text/css">
    <script>
        window.addEventListener("load", function(e) {
            var loaderElement = document.querySelector(".loader-parent");
            if (loaderElement) {
                loaderElement.classList.add("fade-out-loader");
            }
        }, false);

    </script>
</head>

<body>

    <nav>
        <ul>
            <li><a href="./index.php">Home</a></li>
            <li><a href="./admin.php">Admin</a></li>
            <li><a href="">Log out</a></li>
            <li>
                <a href="view_cart.php"><img id="cartIcon" src="assets/shoppingcart/cart.png" alt=""><span class="cart_number"><?= $cart_manager->get_cart_count(); ?></span></a>
            </li>
        </ul>
    </nav>
