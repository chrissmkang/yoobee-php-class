<?php
session_start();

include_once ("classes/db_manager.php");
include_once ("classes/cart_manager.php");


$db = new Connect();
$products = $db->getProducts();

$cart_manager = new Cart_Manager();


include ("includes/header.php");
?>

    <ul>
        <li>Create a database : thriftee</li>
        <li>
            Create a table : user
            <ul>
                <li>user_id : tinyint, unsigned, primary key, unsigned, autoincrement</li>
                <li>first_name : varchar(255) NOT NULL</li>
                <li>last_name : varchar(255) NOT NULL</li>
                <li>email : varchar(255) NOT NULL</li>
                <li>password : varchar(255) NOT NULL</li>
            </ul>
        </li>
        <li>
            Create a table : products
            <ul>
                <li>product_id : tinyint, unsigned, primary key, unsigned, autoincrement</li>
                <li>product_description : varchar(500)</li>
                <li>price : decimal(13,2), as defined: 0.00</li>
                <li>image_path : varchar(500)</li>
                <li>category-id : tinyint</li>
            </ul>
        </li>
        <li>ALTER TABLE products ADD status ENUM('true', 'false') NOT NULL DEFAULT 'true' AFTER catagory_id
        </li>
    </ul>
    <?php
    echo "<div><p>This is what products array looks like</p><pre>";
    var_dump($products[0]);
    echo "</pre></div>";
    ?>
        <div class="container">
            <div class="grid">

                <?php for($i = 0; $i < count($products); $i++) : ?>
                    <div class="cell">
                        <img src="assets/<?php echo $products[$i]['image_path'];?>" class="responsive-image" alt="<?= $products[$i][" product_description "]; ?>">
                        <span><?php echo $products[$i]['product_description'];?></span><span>$<?php echo $products[$i]['price'];?></span>
                        <center>
                            <button>
                                <a href="<?php echo 'classes/update_cart.php?productid=' . $products[$i]['product_id']; ?>" target="_self">
                                    <!--this link takes to update_cart.php wth productid at the end-->
                                    BUY ME NOW
                                </a>
                            </button>
                        </center>
                    </div>
                    <?php endfor; ?>
            </div>
        </div>



        <?php 
include ('includes/footer.php');
?>
