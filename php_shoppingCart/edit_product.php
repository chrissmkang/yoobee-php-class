<?php
session_start();
include_once ("./classes/db_manager.php");
include_once ("./classes/cart_manager.php");

$db  = new Connect();
$cart_manager = new Cart_Manager();
$product = [];

if (isset($_GET["pid"])) {
    $product = $db->get_single_product_for_admin($_GET["pid"]);
}

?>

    <?php include ("./includes/header.php"); ?>
        <script src="tinymce/js/tinymce/tinymce.min.js"></script>

        <h1 style="text-align:center;">Edit Single Item</h1>

        <?php if(count($product)): ?>

            <form action="./classes/update_product.php" enctype="multipart/form-data" method="post">
                <!--The form is connected to update_product.php-->

                <input type="hidden" name="productID" value="<?= $product[0]['product_id']; ?>">

                <fieldset>
                    <img src="./assets/<?= $product[0]["image_path"];?>">
                    <input type="hidden" name="file_original" value="<?= $product[0]['image_path']; ?>">
                    <input type="file" name="file_upload" value="">
                </fieldset>

                <fieldset>
                    <label for="price">PRICE:</label>
                    <input name="price_original" type="hidden" value="<?= $product[0]["price"]; ?>"> Change
                    <input name="price" type="number" step="0.10" min="0.00" value="<?= $product[0]["price"]; ?>">
                </fieldset>

                <textarea id="rtf" name="description" cols="50" rows="4">
                    <?= $product[0]['product_description']; ?>
                </textarea>
                <input type="submit">

            </form>

            <script>
                tinymce.init({
                    selector: '#rtf',
                    height: 200,
                    theme: 'modern',
                    plugins: [
                        'advlist autolink lists link charmap print preview hr anchor pagebreak',
                        'searchreplace wordcount visualblocks visualchars code fullscreen',
                        'insertdatetime nonbreaking save table contextmenu directionality',
                        'template paste textcolor colorpicker textpattern toc'
                    ],
                    toolbar1: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
                    toolbar2: 'print preview | forecolor backcolor emoticons'
                });

            </script>

            <?php else: ?>

                <h1>Your product line is empty!</h1>

                <?php endif; ?>

                    <?php
            echo "<pre>";
                var_dump($_GET);
            echo "</pre>";

?>

                        <?php include ("./includes/footer.php"); ?>
