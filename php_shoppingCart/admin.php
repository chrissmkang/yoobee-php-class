<?php
session_start();
include_once ("./classes/db_manager.php");
include_once ("./classes/cart_manager.php");
include_once ("./classes/admin_manager.php");

$db  = new Connect();
$all_products = $db->get_all_products_for_admin();

$cart_manager = new Cart_Manager();

?>
    <?php include ("./includes/header.php"); ?>

        <h1 style="text-align:center;">Welcome to Admin</h1>

        <?php if (count($all_products)): ?>

            <table class="view_cart_tbl" style="border-collapse: collapse;">
                <thead class="tbl_head">
                    <tr>
                        <th>Product</th>
                        <th>Price</th>
                        <th>Description</th>
                    </tr>
                </thead>

                <tbody class="tbl_body">

                    <?php foreach ($all_products as $product ): ?>
                        <tr>
                            <td> <img src="./assets/<?= $product['image_path']; ?>" alt="" width="120" height="auto"></td>
                            <td><span>$<?= $product['price']; ?> </span></td>
                            <td><span><?= $product['product_description']; ?> </span></td>
                            <td>
                                <button>
                                    <a id="product_<?= $product['product_id']; ?>" href="<?= './classes/admin_manager.php' . '?update_product_status='. $product['product_id']." #prod-{$product[ 'product_id']} ";?>" target="_self">
                                        <?php if($product['status']=="true") :?> <span style="color:darkgreen;">available</span>
                                            <?php else:?><span style="color:crimson;">Not available</span>
                                                <?php endif; ?>
                                    </a>
                                </button>
                            </td>
                               <td>
                                <button>
                                    <a id="product_<?= $product['product_id']; ?>" href="<?= 'edit_product.php' . '?pid='.$product["product_id"] ?>" target="_self">
                                        <span>Edit</span>
                                    </a>
                                </button>
                            </td>
                        </tr>
                        <?php endforeach; ?>


                </tbody>

            </table>
            <?php else: ?>
                <h1>your admin is empty</h1>
                <?php endif; ?>

                    <?php
include ('includes/footer.php');
?>
