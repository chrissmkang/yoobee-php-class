<?php
class File_Manager

{
	private $destination = '../assets/';
	private $fileName = '';
	private $maxSize = '1048576'; // bytes (1048576 bytes = 1 meg)
	private $allowedExtensions = array(
		'image/jpeg',
		'image/png',
		'image/gif'
	); // mime types
	private $error = '';
	private $image_info_array = [];
    
	public function __construct($file)
	{
		// get basic file info
		$infoarr = getimagesize($file["tmp_name"]);

		// store image info for easy access
		$this->image_info_arr = ['width' => $infoarr[0], 'height' => $infoarr[1], 'mime' => $infoarr['mime']];
        
		$this->validate($file);
        

		if ($this->error == '') {
            // we could get in here and change the filename if we want to
			$this->fileName = $file['name'];
            
			move_uploaded_file($file['tmp_name'], $this->destination . $this->fileName) or die('Destination Directory Permission Problem.<br />');
		}
		else {
			echo $this->error;
		}
	}
    
    public function get_new_file_name(){
        if ($this->error == ''){
             return $this->fileName;
        }
        
        return '';
    }
    
    public function set_max_file_size($bytes){
        $this->maxSize = $bytes;
    }

	// PRIVATE VALIDATION:
	private function validate($file)
	{
        // append to errors string as we go
		$error = '';

		// check file name length

		if (strlen($file['name']) < 5) {
			$error.= 'No file found.<br />';
		}

		// check allowed extensions
		if (in_array($this->image_info_arr["mime"], $this->allowedExtensions) === false) {
			$error.= 'Extension is not allowed.<br />';
		}

		// check file size
		if ($file['size'] > $this->maxSize) {
			$error.= 'Max File Size Exceeded. Limit: ' . $this->maxSize . ' bytes.<br />';
		}

		$this->error = $error;
	}
} //end of class

?>
