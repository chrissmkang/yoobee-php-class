<?php 

//session_manager deals anything to do with session storage

class Session_Manager {
    
    public function __construct(){
         
    }
    
    public function update_for_verified_user($username){
        //echo "user verified , updating current session";
        $_SESSION["verified"] = true;
        $_SESSION["username"] = $username;
    }
    
    public function destroy_current_cart(){
        //unset($_SESSION['cart']);
    }

    public function destroy_current_user(){
        unset($_SESSION['verified']);
        unset($_SESSION['username']);
    }
    
    
    public function destroy_session(){
    $_SESSION = [];
    session_destroy();

    header("Location: ../index.php");
    }

}#end of class


?>