<?php 
session_start();
include ("cart_manager.php");

//update_cart.php has functions to update cart data storage and the page

class Remove_Cart {
    
    public function __construct(){
        if (isset($_GET["productid"]) && isset($_SERVER['HTTP_REFERER'])) {
        //Every time 'buy now' is clicked, productid is passed from URL, & if it has a referer,
        //it's going to run update
            $this->remove();
        } else {
            $this->generic_error();
        }
    }
    
    public function remove() {
        //Update our session via the cart_manager
        $cart_manager = new Cart_Manager();
        $cart_manager->remove_product_from_cart($_GET["productid"]);
        //Creates new_product_added obj and puts into $_SESSION["cart"].
        
        header("Location: " . $_SERVER['HTTP_REFERER']);
        //After updating the number, go back to own page
    }
    
    private function generic_error(){
        header("Location: ../index.php");
    }
}

new Remove_Cart();
//Instantiate the class Update_Cart(this page) when the button is clicked

?>
