<?php 

define("DB_SERVER", "127.0.0.1");
define("DB_USER", "root");
define("DB_PASSWORD", "");
define("DB_NAME", "thriftee");

//db_manager.php brings data from mySQl

class Connect {

    private $database;
    
    public function __construct(){
        $this->database = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME); 
    
        if ($this->database->connect_error) {
            echo '<!-- oh dear, db connection is not good -->';
            die ("db error, i'm dead");
            } else { 
            echo '<!-- the db connection is good -->'; 
            } 
    }
    
    public function sql($sql){
        $result = $this->database->query($sql);
        return $result;
    }
    
    public function getProducts() {
        $sql = 'select * from products WHERE products.status LIKE "true" order by product_id ASC';
        //select all data of the products into an array
        
        $result = $this->database->query($sql);
        
        $imageArray = [];
        
        if($result->num_rows > 0) {
            while($row = $result->fetch_assoc()){
                array_push($imageArray, $row);
            }
        }
        return $imageArray;
    }
    
    public function get_cart_products_from_db($list){
        // All info about our cart products;
        $sql = "SELECT * from products where product_id in ($list)"; 
         
        $result = $this->database->query($sql);
        $cartItems = [];
      
        if ( $result && $result->num_rows > 0){
            while ($row = $result->fetch_assoc()) {
                array_push($cartItems, $row);
            }
        }
        return $cartItems;
    }
    
    public function get_all_products_for_admin(){
        $sql = "SELECT * FROM products";
        $result = $this->database->query($sql);

        $allProducts = [];

        if ( $result && $result->num_rows > 0){

            while ($row = $result->fetch_assoc()) {
                array_push($allProducts, $row );
            }
        }
        return $allProducts;
    }
    
    public function get_single_product_for_admin($pid){
        $sql = "SELECT * FROM products WHERE products.product_id=$pid";
        $result = $this->database->query($sql);
        
        $chosenProduct = [];
        
        if ( $result && $result->num_rows > 0){

            while ($row = $result->fetch_assoc()) {
                array_push($chosenProduct, $row );
            }
        }
        return $chosenProduct;
    }
    
    public function update_product($pid, $pp){
        $sql = "UPDATE products SET products.price = $pp WHERE (products.product_id) = $pid";
        $result = $this->database->query($sql);
    }
    
     private function my_escape($str){
        return $this->database->real_escape_string($str);
    }
    
    public function update_product_record($productID, $price, $description, $imagepath){
        
        $description = $this->my_escape($description);
        $price = $this->my_escape($price);
        
        $sql = "UPDATE products SET product_description='$description', price='$price', image_path='$imagepath' WHERE (product_id=$productID)"; 
        echo "update_product_record " . $sql;
        $this->database->query($sql);
    }
    
}

?>
