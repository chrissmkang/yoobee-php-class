<?php 
session_start();
include ("cart_manager.php");

//update_cart.php has functions to update cart data storage and the page

class Update_Cart {
    
    public function __construct(){
        if (isset($_GET["productid"]) && isset($_SERVER['HTTP_REFERER'])) {
        //Every time 'buy now' is clicked, productid is passed from URL, & if it has a referer,
        //it's going to run update
            $this->update();
        } else {
            $this->generic_error();
        }
    }
    
    public function update() {
        //Update our session via the cart_manager
        $cart_manager_obj = new Cart_Manager();
        $cart_manager_obj->save_product_to_cart($_GET["productid"]);
        //Creates new_product_added obj and puts into $_SESSION["cart"].
        
        $this->return_to_sender();
        //After updating the number, go back to own page
    }
    
    private function return_to_sender() {
        header("Location: " . $_SERVER['HTTP_REFERER']);
        // Without this header, the page will just show arrays on page.
    }
    
    private function generic_error(){
        header("Location: ../index.php");
    }
}

new Update_Cart();
//Instantiate the class Update_Cart(this page) when the button is clicked

?>
