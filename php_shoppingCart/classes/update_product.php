<?php

include_once ("db_manager.php");
include_once ("file_manager.php");
 

class Update_Product {
    
    private $image_path;
    //empty variable to be assigned in __construct();
 
    public function __construct(){
        $productID = $_POST["productID"];
        $price = $this->sanitize_user_input($_POST["price"]);
        $description = $this->sanitize_user_input($_POST["description"]);
        //sanitize price and user input

        if ( is_array($_FILES) && isset($_FILES['file_upload'])  && strlen($_FILES["file_upload"]["name"])) {
        // do we have a new file choice?
        // $_FILES is null unless the user chooses a new file via the dialogue box
            $file_manager = new File_Manager($_FILES['file_upload']);
            // this method could return an empty string if file is too large or wrong type
            $this->image_path = $file_manager->get_new_file_name();
            //validate the image file using file_manager, and set the new image path to the final file_name.
        } 
        
        //last minute check on filename
        if ( ($this->image_path == '') OR empty($this->image_path)) {
        //if the file is not uploaded, use the original
            $this->image_path = $_POST['file_original'];
        }
        
        
        $database_manager  = new Connect();
        $database_manager->update_product_record($productID, $price, $description, $this->image_path);
        
        
        
        header("Location: ../admin.php");
        exit;
  }
    
    
    private function sanitize_user_input($str){
        $temp = trim($str);
        //$temp = filter_var( $temp , FILTER_SANITIZE_STRING);
        return $temp;
    }
     
    
 }#end of class


new Update_Product();

?>
