<?php 

//cart_manager.php will have methods for update_cart through $_SESSION


class Cart_Manager {
    
    public function __construct() {
        if ( !isset($_SESSION["cart"]) ){
            //When Session_Manager is instanciated for the first time, create a an empty array in session called = cart
            $_SESSION["cart"] = [];
        }    
    }
    
    public function get_user_cart(){
        //Show whats in cart
        return $_SESSION["cart"];
	}

    public function get_cart_count(){
        //Print out number of cart items
        if (isset($_SESSION["cart"])){
            echo count($_SESSION["cart"]);
        }
    }
        
    public function save_product_to_cart($productID){
        //Adding new productID input to $_SESSION["cart"];
        $new_product_array = ['product_id' => $productID, 'product_count'=>0];
        
        array_push($_SESSION["cart"], $new_product_array);
        //Push $new_product_added object into $_SESSION["cart"].
    
  	}
    
    public function remove_product_from_cart($productID){
        //Get the productID of the selected item
        foreach($_SESSION["cart"] as $i){
            //Looping $_SESSION["cart"][$i]
            
            if($i["product_id"] == $productID){
                //if product_id in cart == selected item
                $array_int = array_search($i, $_SESSION["cart"]);
                //search the nth number of array in $_SESSION["cart"]
                unset($_SESSION["cart"][$array_int]);
                //Unset the nth array in $_SESSION["cart"]
                break;
                //exit out of it
            }
        }
  	}
    
	public function get_qt_for_products($productID){
        $matches = 0;
        
        foreach($_SESSION["cart"] as $product){
        //Each items in 'cart' is $product
            if($product["product_id"] == $productID){
                $matches++;
            }
        }
        return $matches;
    }
    
    public function get_TOT_price_each_item($cartItem, $i){
        $price = $cartItem[$i]['price'];
        $qt = $this->get_qt_for_products($cartItem[$i]['product_id']);
        
        $total = $price * $qt;
        
        return $total;
        
    }
    
}


?>
