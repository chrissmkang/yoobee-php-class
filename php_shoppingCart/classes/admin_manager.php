<?php 
include_once("db_manager.php");

class Admin_Manager {
    
    public function __construct(){
        if (isset($_GET["update_product_status"]) && isset($_SERVER['HTTP_REFERER'])) {
            $this->toggle_product_toggle($_GET["update_product_status"]);
        }
    }
    
    public function toggle_product_toggle($productID){
         if (isset($_GET["update_product_status"]) && isset($_SERVER['HTTP_REFERER'])) {
             $sql = "UPDATE products SET status=(SELECT CASE status WHEN 'true' THEN 'false' ELSE 'true' END) WHERE product_id = $productID";
             
             $db = new Connect();
             
             $result = $db->sql($sql);
             
             header("Location: ../admin.php");
         }
    }
        
}

new Admin_manager();

?>
