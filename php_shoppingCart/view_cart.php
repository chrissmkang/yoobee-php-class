<?php 

session_start();

include_once ("./classes/db_manager.php");
include_once ("./classes/cart_manager.php");

$db = new Connect();
$cart_manager = new Cart_Manager();

$cart_products = $cart_manager->get_user_cart();

if (count($cart_products)) {
    
    // add product_id to another array so we can implode the new array and send as a string to mysql
    $id_array = [];
    
    foreach ($cart_products as $product){
        array_push($id_array, $product["product_id"]);
    } 
    
    $id_array = array_unique($id_array);
    //remove duplicates from array before we form the sql string. Not neccessary.
    
    $list = implode("," , $id_array);
    # $list will end up as a string like "1,3,4,5,8"

    $cart_tbl_array = $db->get_cart_products_from_db($list);
    
} else {
    
    $cart_tbl_array = [];
}

?>

    <?php
include ('includes/header.php');
?>
        <table class="view_cart_tbl">

            <thead class="tbl_head">
                <tr>
                    <th>Product</th>
                    <th>Price</th>
                    <th>Description</th>
                    <th>Quantity</th>
                    <th>Total Price</th>
                    <th></th>
                </tr>
            </thead>
            <tbody class="tbl_body">

                <?php $totalPrice = 0; ?>
                    <?php if(count($cart_tbl_array)): ?>
                        <?php for($i=0; $i<count($cart_tbl_array); $i++) : ?>
                            <tr>
                                <td><img src="./assets/<?= $cart_tbl_array[$i]['image_path'];?>" alt=""></td>
                                <td>$
                                    <?= $cart_tbl_array[$i]['price']; ?>
                                </td>
                                <td>
                                    <?= $cart_tbl_array[$i]['product_description']; ?>
                                </td>
                                <td>
                                    <?= $cart_manager->get_qt_for_products($cart_tbl_array[$i]['product_id']);?>
                                </td>
                                <td>$
                                    <?php echo $cart_manager->get_TOT_price_each_item($cart_tbl_array, $i); 
                                                                  
                                $totalPrice += $cart_manager->get_TOT_price_each_item($cart_tbl_array, $i);
                                ?>.00</td>
                                <td>
                                    <button>
                                        <a href="<?= './classes/remove_cart.php' . '?productid='. $cart_tbl_array[$i]['product_id']; ?>">
                                        remove
                                        </a>
                                    </button>
                                </td>
                            </tr>
                            <?php endfor;?>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="totalPrice">$
                                        <?= $totalPrice; ?>.00</td>
                                    <td></td>
                                </tr>


                                <?php else: ?>

                                    <div class="emptycart">Your Cart is Empty</div>

                                    <?php endif; ?>
            </tbody>
        </table>





        <?php
include ('includes/footer.php');
?>
