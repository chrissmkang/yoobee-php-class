<?php

define("DB_SERVER", "127.0.0.1");
define("DB_USER", "root");
define("DB_PASSWORD", "");
define("DB_NAME", "toDos");


class Connection {
    private $mysqli_obj; // handles all sql statements not just select
    public $row_count = 0;

    // magic method - called on object birth.

    public function __construct() {
        $this->mysqli_obj = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME);
        //mysqli = prebuilt class in php

        // connect_error is a property of mysqli_obj - only exists if there's a problem. If all is good its NULL;

        if ($this->mysqli_obj->connect_error) {
            echo '<!-- oh dear, db connection is not good -->';
            die("db error, I'm out of here");
        } else {
            echo '<!-- happy days, the db connection is good -->';
            }
    }

    // magic method - called on object death.
    public function __destruct() {
        echo 'closing db     connection<br>';
        // cleaning up our open DB connection
        $this->mysqli_obj->close();
            }


    // execute the passed in insert, alter, or delete query and return result obj
    public function insert_query($cleanedUserText) {
        
        $sql = 'INSERT INTO todos VALUES (NULL,"'.$cleanedUserText.'", false )';

        // returns only boolean for an insert query
        //If succeeded to insert, boolean(true)
        //If failed to insert, boolean(false)
        $result = $this->mysqli_obj->query($sql);
        
        if ($result == true) {
            echo 'sql insert has worked<br>';
        }
        else {
            echo 'sql insert failed<br>';
        }
    }
    
    
    public function getRowCount() {
        return $this->row_count;
    }
    
} // end of connection class

?>
