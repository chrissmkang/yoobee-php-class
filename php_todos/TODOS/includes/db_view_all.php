<?php

define("DB_SERVER", "127.0.0.1");
define("DB_USER", "root");
define("DB_PASSWORD", "");
define("DB_NAME", "toDos");

class Connection

{
    private $mysqli_obj; // handles all sql statements not just select
    public $row_count = 0;

    // magic method - called on object birth.

    public function __construct()
    {
        $this->mysqli_obj = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME);

        // connect_error is a property of mysqli_obj - only exists if there's a problem. If all is good its NULL;

        if ($this->mysqli_obj->connect_error) {
            echo '<!-- oh dear, db connection is not good -->';
            die("db error, I'm out of here");
        }
        else {
            echo '<!-- happy days, the db connection is good -->';
        }
    }

    // magic method - called on object death.

    public function __destruct()
    {
        echo '<br>closing db connection';

        
        // cleaning up our open DB connection
        $this->mysqli_obj->close();
    }

    // execute the passed in select query and return result

    public function select_query($sql)
    {

        // execute query and get a mysqli_obj results obj
       
        // clean-up SQL for insertion
        $cleanedSQL = $this->mysqli_obj->real_escape_string($sql);

        // execute SQL
        $result = $this->mysqli_obj->query($cleanedSQL);

        //dumpObjPretty($result);
        
        
        if ($result && $result->num_rows ) {
            // lets store our row count in this object
            $this->row_count = $result->num_rows;
        }
        else {
            echo "no results from query";
        }

        return $result;
    }

    // fetch the data collection from result_set

    public function fetch($resultSet)
    {
        
        // !$result->dataSeek(0);
        return $resultSet->fetch_assoc();
    }

    public function getRowCount()
    {
        return $this->row_count;
    }
} // end of connection class

?>