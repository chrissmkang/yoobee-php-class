<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <style>
        * {
            font-family: helvetica, sans-serif;
        }
        
        h1 {
            text-align: center;
        }
        
        p {
            font-weight: bold;
        }
        
        .code-block {
            background-color: #333;
            color: #fff;
            padding: 1em;
        }
        
        code {
            font-family: monospace;
        }

    </style>
    <title>
        <?php echo 'This is a php title' ?>
    </title>
</head>

<body>
    <h1>Basics of PHP</h1>
    <ul>
        <li>
            <p>php files can have HTML document WITH php block</p>
            <div class="code-block"><code> &lt;?php echo '&lt;button>This is a php button block&lt;/button>' ?></code></div>
            <?php echo '<button>This is a php button block</button>' ?>
        </li>
        <li>
            <p>Double quotation marks are used only or variables</p>
            <div class="code-block">
                <code>
                       &lt;?php <br>
                        $firstName = "Chris";<br>
                        $lastName = "Kang";<br>
                        <br>
                        echo 'My name is $firstName $lastName&lt;br>';<br>
                        echo 'My name is '."$firstName"." $lastName";<br>
                    ?><br>
                    </code>
            </div>
            <?php 
                        $firstName = "Chris";
                        $lastName = "Kang";
                        
                        echo 'My name is $firstName $lastName<br>';
                        echo 'My name is '."$firstName"." $lastName";
                    ?>
        </li>
        <li>
            <p>Double quotes in PHP will interpret your PHP code - it will output variables:</p>

            <div class="code-block">
                <code> &lt;?php<br>
                            # wise to name your data type<br>
                            # vars are case sensitive and usual naming<br> rules apply<br>
                            <br>
                            $str_first_var = "Hello, I am a variable";<br>
                            $str_second_var = "I am a second variable";<br>
                            $num_one = 100;<br>
                            $num_two = 150;<br>
                            $num_three =  $num_two - $num_one;<br>
                            <br>
                            // single quotes echoes as what it's written<br>
                            echo '$str_first_var';<br>
                            echo '&lt;br>';<br>
                            // concat<br>
                            echo "$str_first_var";<br>
                            echo '&lt;br>';<br>
                            echo "$str_second_var";<br>
                            echo '&lt;br>'; <br>
                            echo 'Calcuation is ' . $num_three;<br>
                            echo '&lt;br>';<br>
                            <br>
                            echo " $str_first_var" . " ----- " . "$str_second_var";<br>
                            echo '&lt;br> OR &lt;br>';<br>
                            echo "$str_first_var ----- $str_second_var";<br>
                            ?>&lt;/code>&lt;/div><br>
            <?php
        # wise to name your data type
        # vars are case sensitive and usual naming rules apply
        
        $str_first_var = "Hello, I am a variable";
        $str_second_var = "I am a second variable";
        $num_one = 100;
        $num_two = 150;
        $num_three =  $num_two - $num_one;
        
        // single quotes echoes as what it's written
        echo '$str_first_var';
        echo '<br>';
        // concat
        echo "$str_first_var";
        echo '<br>';
        echo "$str_second_var";
        echo '<br>'; 
        echo 'Calcuation is ' . $num_three;
        echo '<br>';
        
        echo " $str_first_var" . " ----- " . "$str_second_var";
        echo '<br> OR <br>';
        echo "$str_first_var -----  $str_second_var";
        ?>
        </li>
        <li>
            <p>Using dates in php</p>
            <div class="code-block"><code>
                           &lt;?php
                           <br> date_default_timezone_set("Pacific/Auckland");<br>
                            echo "Hello, the year is:" . date("Y") . "&lt;br>";<br>
                            echo "Hello, the month name is:" . date("F"). "&lt;br>";<br>
                            echo "Hello, the day name is:" . date("j"). "&lt;br>";<br>
                            echo 'no more comments, thanks.'; # ok thats enough;<br>
                    ?></code></div>
            <?php
        date_default_timezone_set("Pacific/Auckland");
        echo "Hello, the year is:" . date("Y") . "<br>";
        echo "Hello, the month name is:" . date("F"). "<br>";
        echo "Hello, the day name is:" . date("j"). "<br>";
        echo 'no more comments, thanks.'; # ok thats enough;
?>
        </li>
        <li>
            <p>Using timezone </p>
            <div class="code-block"><code> 
                       &lt;?php<br>
                        date_default_timezone_set("Pacific/Auckland");
                            echo "Hello, the time is:" . date("H:i:s a."). "&lt;br>";<br>
                        date_default_timezone_set("Europe/Berlin");
                            echo "Hello, the time is:" . date("H:i:s a."). "&lt;br>";<br>
                            echo 'no more comments, thanks.'; # ok thats enough;<br>
                    ?></code></div>
            <?php
    date_default_timezone_set("Pacific/Auckland");
        echo "Hello, the time is:" . date("H:i:s a."). "<br>";
    date_default_timezone_set("Europe/Berlin");
        echo "Hello, the time is:" . date("H:i:s a."). "<br>";
        echo 'no more comments, thanks.'; # ok thats enough;
?>
        </li>
        <li>
            <p>Numbers</p>
            <div class="code-block"><code>
                   &lt;?php<br>
                    <br>
                            $num_var_one = 99.99768867;<br>
                            $num_var_two = 100;<br>
                    <br>
                            echo round($num_var_one + $num_var_two);<br>
                            echo '&lt;br>' . gettype($num_var_one);<br>
                            echo '&lt;br>' . gettype($num_var_two) . '&lt;br>' ;<br>
                            echo  is_string( $num_var_one ) === true ? "true": "false";<br>
                    ?></code></div><?php

        $num_var_one = 99.99768867;
        $num_var_two = 100;

        echo round($num_var_one + $num_var_two);
        echo '<br>' . gettype($num_var_one);
        echo '<br>' . gettype($num_var_two) . '<br>' ;
        echo  is_string( $num_var_one ) === true ? "true": "false";
?>
        </li>
    </ul>

</body>

</html>
