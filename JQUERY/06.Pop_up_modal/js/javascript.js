$(function () {
    console.log("dom loaded")

    $('.modalControl').on('click', function (e) {
        e.preventDefault(); //stop the link

        $('.overlay').removeClass('hide');
        setTimeout(function () {
            $('.overlay').addClass('fadeIn');
        }, 1);


        $('.modal').removeClass('hide');
        setTimeout(function () {
            $('.modal').addClass('fadeIn');
        }, 1);
        

    });

    $('.modalDismiss').on('click', function () {
        $('.overlay').removeClass('fadeIn');

        $('.overlay').one('transitionend', function () {
            console.log("display:none on overlay");
            $(this).addClass('hide');
        });

        $('.modal').removeClass('fadeIn');

        $('.modal').one('transitionend', function () {
            // listen for the end of the transition from the CSS, once it fires hide
            // the element properly with a display:none
            console.log("display:none on modal");
            $(this).addClass('hide');
        });

    });


});