-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 14, 2016 at 06:25 am
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mylogin`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`user_id` int(11) unsigned NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `signupdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `signupdate`) VALUES
(1, 'dsfsdf', '$2y$10$6wHsQXdTLXueOWg9nWXPTOLh4seLS.nqrBqnfUKOfDCjoikJ3m4Ie', '2016-10-14 15:34:53'),
(2, '$username', '$hashed', '2016-10-14 15:36:07'),
(5, 'hjghjg', '$2y$10$WZIOzhNQK/3/CizSZnOpY.KhNlldjj6ykBgpY7BTyQAuUFxH/66ya', '2016-10-14 15:38:00'),
(7, 'fsdfsdf', '$2y$10$ALv6tnTOCqanu3wf1CjVBew6cQRs1Kk9olI.Mb/mBCKBY8FjNN7JO', '2016-10-14 15:40:56'),
(8, 'fsdfsdfdsf', '$2y$10$zbWUuBJMgLrlCQbnwrChj.I258WpeiW16EoOVZp16D3dcc1ZgxvBm', '2016-10-14 15:45:10'),
(9, 'sdfsdfs', '$2y$10$GPX8VoBl9ODDihwoGvuBBuQwkKkJJekaP5MKuQ.5VatADcq3ifC..', '2016-10-14 15:52:47'),
(10, 'fdgdfg', '$2y$10$fqEA4vrLd7Z3cu8iPoI4ueWfSYv0Z2CgW344FyM349srBI7z9OGjC', '2016-10-14 15:54:37'),
(11, 'sfdsfsd', '$2y$10$CMOL2IHZl9BhqdIRAKjnP.rwaaiiQ5ZCrwq0jy7lN6VzxI0uS/opm', '2016-10-14 15:56:46'),
(12, 'fsdfdsf', '$2y$10$GIS324qIAXPQu9xZux/aXOPI2NzAUFZAh0I74YHCk/K.PS.JBuNu2', '2016-10-14 16:08:56'),
(13, 'dasdasd', '$2y$10$V7XoXA8uSyhAuL9KueCZCOybMZ6FTcnKMjx6NXCqejfwi3miihfFu', '2016-10-14 16:09:17'),
(15, 'fgfd', '$2y$10$mPvp2Frtp4sQxE6yGurMeOYRn0GhWp5Srve8Mva8CGuAwGAUsS1Ui', '2016-10-14 16:14:29'),
(17, 'gdfgdfgd', '$2y$10$R7mcY1kshF3bxKpEP5gLUuuOSaWqHPDlaP3iPQfjI1vjeO9UV3OA6', '2016-10-14 16:14:53'),
(18, 'dasdasdasdas', '$2y$10$s.0obQAMu4QoKJX499XfgOx/d6SuSKqFDZSTU7Twz3.mGPG8Wsvta', '2016-10-14 16:16:28'),
(19, 'dasdasdas', '$2y$10$yBywwo.P91MXTACXtIWn9.P3QoOfBTK93Ec4AUoP1T1o4wrYYJHUO', '2016-10-14 16:16:36'),
(20, 'dasdasasd', '$2y$10$b6FyHRgCI79RK.FSManA6uNM4XoT3CvXKLT5cphC5/4G6vglcpY3a', '2016-10-14 16:16:55'),
(21, 'hfghfghfghfgh', '$2y$10$Exib8FG4Q3jJIQUqSwQhS.n3z2DkEtIHIwKbbGSQGZSiHdkxjkjJa', '2016-10-14 16:18:23'),
(22, 'gfhfghfg', '$2y$10$34J9YoWXNUxJdVzOtsSsD.u70Q2GFn2OV3itHBNI9kKWzwk9mCgBu', '2016-10-14 16:18:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`user_id`), ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
