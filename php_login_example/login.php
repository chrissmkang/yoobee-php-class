<?php

session_start();

if (isset($_POST["submit-logout"] )) {
    echo "killing session";
    $_SESSION = [];
    session_unset();
}  


if ( isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] !== true){
    echo "user is  not logged on <br />";
} elseif ( isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true)  {
    echo "user is logged on <br />";
}


?>


    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">



        <title>New HMTL document by NewJect</title>

        <link href="css/main.css" rel="stylesheet">
        <link href="css/pure.min.css" rel="stylesheet">

        <style>
            .my-form {
                /*                outline: 2px solid red;*/
                width: 300px;
                height: auto;
                margin: auto;
                margin-top: 50px;
            }

            .my-form * {
                margin: 12px;
            }

            fieldset {
                background-color: rgba(0, 0, 0, 0.2)
            }

            legend {
                background-color: rgba(255, 255, 255, 0.2);
                padding: 2px 10px;
            }
        </style>

    </head>

    <body>

        <div class="flex-container">
            <div class="flex-item"><a href="page1.php" target="_self">1</a></div>
            <div class="flex-item"><a href="page2.php" target="_self">2</a></div>
            <div class="flex-item"><a href="page3.php" target="_self">3</a></div>
            <div class="flex-item">
                <a href="" target="_self">
                    <?= isset($_SESSION[ "loggedin"] ) ? "log-out" : "log-in"; ?>
                </a>
            </div>
        </div>

        <?php if ( isset($_SESSION["loggedin"]) ): ?>
        <?php
        echo "<pre>";
        print_r($_SESSION);
        echo "</pre>";
        
        ?>
        
        <form class="my-form" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" class="pure-form pure-form-stacked">
            <fieldset>
                <legend>Log out:</legend>
                <label for="username-field">Username: <span class="red-text"><?= $_SESSION["username"]; ?></span></label>
                <input type="submit" name="submit-logout" class="pure-button pure-button-primary" value="Logout">
            </fieldset>
        </form>

            <?php else: ?>
                 
                <form class="my-form" action="scripts/process_login.php" method="post" class="pure-form pure-form-stacked">
                    <fieldset>
                        <legend>Log in here:</legend>

                        <label for="username-field">Username:</label>
                        <input type="text" value="" name="username-field" maxlength="50">

                        <label for="password-field">Password:</label>
                        <input type="password" value="" name="password-field" maxlength="30">


                        <input type="submit" name="submit-login" class="pure-button pure-button-primary" value="Login">
                    </fieldset>
                </form>


            <?php endif; ?>

    </body>

    </html>
