<?php
session_start();
date_default_timezone_set("Pacific/Auckland");
//session_unset;
 
//echo "<pre>";
//echo var_dump($_POST);
//echo "</pre>";

//empty() does not generate a warning if the variable does not exist.
if ( isset( $_POST["submit-login"]) && !empty( $_POST["password-field"]) && !empty($_POST["username-field"] ) ) {

    // $db = new mysqli("127.0.0.1", "admin", "testpass", "mylogin");
    $db = new mysqli("127.0.0.1", "root", "", "mylogin");
    
    
    if ($db->connect_error){
        die("Connection failed: " . $db->connect_error);
            
    } else {
        
        $hashed = password_hash( $_POST["password-field"],  PASSWORD_DEFAULT );
        $username = filter_var($_POST["username-field"], FILTER_SANITIZE_STRING);
        
        //sql 
        $sql = "INSERT  INTO users( username, password, signupdate) VALUES (\"$username\", \"$hashed\", NOW() )";
        
        
        if ( $db->query($sql)  ) {
            updateSession($username);
            returntoLoginPage();
        } else {
            echo("Error description: " . mysqli_error($db));
        }
        
    }
}
    
function updateSession(&$username){
    $_SESSION["loggedin"] = TRUE;
    $_SESSION["username"] = $username;
}


function returntoLoginPage(){
//   echo "<pre>";
//   var_dump($_SERVER);
//   echo "</pre>";
  header("Location: {$_SERVER['HTTP_REFERER']}");
}


?>