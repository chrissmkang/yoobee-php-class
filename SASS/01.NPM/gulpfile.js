var gulp = require('gulp');
var w3cjs = require('gulp-w3cjs');
var concat = require('gulp-concat');
var browserSync = require('browser-sync');
var prefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var notify = require('gulp-notify');

gulp.task('html-validate', function () {
    //using gulp-w3cjs
    return gulp.src('*.html')
        .pipe(w3cjs())
        .pipe(w3cjs.reporter())
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('scripts', function () {
    //using gulp-concat
    return gulp.src('dev_js/*.js')
        .pipe(concat('all.js'))
        .pipe(gulp.dest('prod_js'))
        .pipe(notify("Javascript Updated"))
        .pipe(browserSync.reload({
            stream: true
        }));;
})

gulp.task('browser-sync', function () {
    //using browser-sync
    browserSync.init({
        server: {
            baseDir: "./"
        },
        browser: ["google chrome"]
    });
})

gulp.task('sassy', function () {
    //using gulp-sass
    return gulp.src('dev_css/*.scss')
        .pipe(sass({
            outputStyle: 'expanded',
            precision: 2,
            errLogToConsole: true
        }))
        .on('error', notify.onError("Error:<%= error.message %>"))
        .pipe(prefixer())
        .pipe(notify("SASS updated CSS"))
        .pipe(gulp.dest('prod_css/'))
        .pipe(browserSync.reload({
            stream: true
        }));;
})

gulp.task('watch', ['browser-sync', 'html-validate', 'scripts', 'sassy'], function () {
    gulp.watch('*.html', ['html-validate']);
    gulp.watch('dev_js/*.js', ["scripts"]);
    gulp.watch('dev_css/*.scss', ["sassy"]);
});

gulp.task('default', ['watch']);
