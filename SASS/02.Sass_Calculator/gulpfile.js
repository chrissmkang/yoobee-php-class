var gulp = require('gulp');
var sass = require('gulp-sass');
var prefixer = require('gulp-autoprefixer');
var browsersync = require('browser-sync');
var notify = require('gulp-notify');

gulp.task('browserSync', function () {
    browsersync.init({
        server: {
            baseDir: './'
        },
        browser: ['google chrome']
    });
})

gulp.task('sass', function () {
    return gulp.src('dev/css/*.scss')
        .pipe(sass({
            outputStyle: 'expanded',
            precision: 2,
            errLogToConsole: true
        }))
        .on('error', notify.onError("Error: <%= error.message %>"))
        .pipe(prefixer())
        .pipe(gulp.dest('prod/css/'))
        .pipe(browsersync.reload({
            stream: true
        }))
})

gulp.task('html-reload', function () {
    return gulp.src('*.html')
        .pipe(browsersync.reload({
            stream: true
        }))
})

gulp.task('watch', ['browserSync', 'sass'], function () {
    gulp.watch('*.html', ['html-reload']);
    gulp.watch('dev/css/*.scss', ['sass']);
});

gulp.task('default', ['watch']);
