var gulp = require('gulp');
var notify = require('gulp-notify');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');

var globbingPath = {
    css: {
        src: 'scss/*.scss',
        dest: "css"
    }
}

gulp.task('sass', function () {
    return gulp.src(globbingPath.css.src)
        .pipe(sass({
            outputStyle: 'extended',
            precision: 2,
            errLogToConsole: true
        }))
        .on('error', notify.onError("Error: <%= error.message %>"))
        .pipe(gulp.dest(globbingPath.css.dest))
        .pipe(browserSync.reload({
            stream: true
        }))
})

gulp.task('browsersync', function () {
    browserSync.init({
        server: {
            baseDir: "./"
        },
        browser: ["google chrome"]
    })
})

gulp.task('refresh', function () {
    return gulp.src('*.html')
        .pipe(browserSync.reload({
            stream: true
        }))
})

gulp.task('watch', ['browsersync', 'sass'], function () {
    gulp.watch('*.html', ['refresh']);
    gulp.watch(globbingPath.css.src, ['sass']);

});

gulp.task('default', ['watch']);
