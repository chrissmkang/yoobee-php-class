<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <style>
        * {
            font-family: helvetica, sans-serif;
        }
        
        h1 {
            text-align: center;
        }
        
        p {
            font-weight: bold;
        }
        
        .code-block {
            background-color: #333;
            color: #fff;
            padding: 1em;
        }
        
        code,
        code * {
            font-family: monospace;
        }
        
        .imp {
            color: yellow;
        }

    </style>
    <title>
        <?php echo 'This is a php title' ?>
    </title>
</head>

<body>
    <h1>Basics of PHP</h1>
    <ul>
        <li>
            <p>Arrays and Arrays with Keys</p>
            <div class="code-block"><code>
                &lt;?php<br>
                $myArray = ['a','b','c','d'...];<br>
                $myArray[] = 'e'; //Adds 'e' to $myArray;<br><br>
                <span class="imp">for</span> ($i = 0; $i &lt; count($myArray); i++) {<br>
                &nbsp; echo '&lt;ul> &lt;li> . $myArray [$i]. '&lt;/li>&lt;/ul>';<br><br> OR <br><br>
                <span class="imp">foreach</span>($myArray <span class="imp">as</span> $list){<br>
                &nbsp;echo '&lt;ul>&lt;li> $list &lt;/li>&lt;/ul>'}
                }
                ?>
            </code></div>
            <?php
        $aNames = ["Whitney","Victoria","Priscilla","Chris","Saerom", "Carl", "Robert", "Alex", "Radu"];
        $aNames[] = "Heidi";
        echo gettype($aNames);
        echo '<br>'. is_array($aNames);
        echo '<br>'. count($aNames);
        for($i= 0; $i<count($aNames); $i++){
            echo '<ul><li>' .$aNames[$i]. '</li></ul>';
        }
        echo '<hr>';
//    foreach to loop an array
        foreach($aNames as $person){
            echo "<ul><li>$person</li></ul>";
        }
        $bNames = [1=>"Whitney", 2=>"Victoria", 3=>"Priscilla", 4=>"Chris", 5=>"Saerom", 6=>"Carl", 7=>"Robert", 8=>"Alex", 9=>"Radu", 10=>"Heidi"];
        $bNames[12] = "Kevin";
        echo '<ul>';
        foreach($bNames as $key => $person){
            echo '<li>'.$key. '------'. $person.'<br></li>';
        }
        echo '</ul>';
        echo"<pre>";
        print_r($bNames);
        echo"</pre>";
?>
        </li>
        <li>
            <p>Form</p>
            <div class="code-block"><code>
            $myArray = ['a','b','c',...];<br>
            echo '&lt;select name="aNames">';<br><br>
                    <span class="imp">for</span> ($i=0; $i&lt;count($myArray); $i++){<br>
                        &nbsp;echo "&lt;option value=&#92;"$myArray[$i]&#92;">$myArray[$i]&lt;/option>";<br>
                    }<br><br>
                echo '&lt;/select>';
                </code></div>
            <form>                
                <?php
        $aNames = ["Whitney","Victoria","Priscilla","Chris","Saerom", "Carl", "Robert", "Alex", "Radu"];
        $aNames[] = "Heidi";
                
        echo '<select name="aNames">';
            for ($i=0; $i<count($aNames); $i++){
                echo "<option value=\"$aNames[$i]\">$aNames[$i]</option>";
            }
        echo '</select>';
                
        echo '<br>OR<br>';
                
        echo '<select name="aNames">';
            foreach($aNames as $person){
                echo "<option value=\"$person\">$person</option>";
            }
        echo '</select>';

    ?>
            </form>
        </li>
        <li>
            <p>Super Global array of Server</p>
            <?php
        echo "<pre>";
#print_r a special form of echo
        print_r($_SERVER); #for debugging
        echo "</pre>";
        echo '<hr>';
        echo '<p>my relative script path is = <span style="color:red;">'.$_SERVER["SCRIPT_NAME"]. '</span>';
        echo '<p>my relative host ID is = <span style="color:red;">'.$_SERVER["HTTP_HOST"]. '</span>';
        echo '<hr>';
            
            
    ?>
        </li>
        <li>
            <p>Super Global Array of Global</p>
            <span>$_SERVER Variables set by the web server or otherwise directly related to the execution environment of the current script.</span>
            <br>
            <span>$_GET Variables provided to the script via URL query string</span>
            <br>
            <span>$_POST Variables provided to the script via HTTP POST.</span>
            <br>
            <span>$_ENV Variables provided to the script via the environment</span>
            <br>
            <span>$_SESSION Variables which are currently registered to a script's session</span>
            <br>
            <?php 
            echo "<pre>";
                print_r($GLOBALS);
            echo "</pre>";
            ?>
        </li>
        <li>
            <p>Selecting an array from Super Global</p>
            <span>***First, add <strong><u>?firstName=Chris</u></strong> at the end of URL***</span>
            <?php
            echo "<pre>";
                print_r($_GET["firstName"]);
            echo "</pre>";
            ?>
        </li>
        <li>
            <p>To cure the error without typing an element into URL, use</p> <code>if (isset($_GET['firstName'])){ print_r($_GET['firstName']); };</code>
            <?php
            echo "<pre>";
            if (isset($_GET['firstName'])){
                print_r($_GET['firstName']);
            };
            echo "</pre>"; ?>
        </li>
    </ul>

</body>

</html>
